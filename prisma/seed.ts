import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()
async function main() {
    const super_admin = await prisma.user.upsert({
        where: { id: 1 },
        update: {},
        create: {
            name: '超级管理员',
            username: 'super_admin',
            password: '123456',
            organization_model: "all"
        },
    })
    const super_admin_role = await prisma.role.upsert({
        where: { id: 1 },
        update: {},
        create: {
            name: '超级管理员',
            code: 'SUPER_ADMIN',
            description: '超级管理员,拥有所有权限',
            //   permissions: {
            //     set: [
            //       {
            //         code: 'USER_CREATE',
            //         description: '创建用户',
            //       },
            //       {
            //         code: 'USER_UPDATE',
            //         description: '更新用户',
            //       },
            //       {
            //         code: 'USER_DELETE',
            //         description: '删除用户',
            //       },
            //       {
            //         code: 'ROLE_CREATE',
            //         description: '创建角色',
            //       },
            //       {
            //         code: 'ROLE_UPDATE',
            //         description: '更新角色',
            //       },
            //       {
            //         code: 'ROLE_DELETE',
            //         description: '删除角色',
            //       },
            //       {
            //         code: 'PERMISSION_CREATE',
            //         description: '创建权限',
            //       },
            //       {
            //         code: 'PERMISSION_UPDATE',
            //         description: '更新权限',
            //       },
            //       {
            //         code: 'PERMISSION_DELETE',
            //         description: '删除权限',
            //       },
            //       {
            //         code: 'ORGANIZATION_CREATE',
            //         description: '创建组织',
            //       },
            //       {
            //         code: 'ORGANIZATION_UPDATE',
            //         description: '更新组织',
            //       },
            //       {
            //         code: 'ORGANIZATION_DELETE',
            //         description: '删除组织',
            //       },
            //       {
            //         code: 'PROJECT_CREATE',
            //         description: '创建项目',
            //       },
            //       {
            //         code: 'PROJECT_UPDATE',
            //         description: '更新项目',
            //       },
            //       {
            //         code: 'PROJECT_DELETE',
            //         description: '删除项目',
            //       },
            //       {
            //         code: 'TASK_CREATE',
            //         description: '创建任务',
            //       },
            //       {
            //         code: 'TASK_UPDATE',
            //         description: '更新任务',
            //       },
            //       {
            //         code: 'TASK_DELETE',
            //         description: '删除任务',
            //       },
            //       {
            //         code: 'ATTACHMENT_CREATE',
            //         description: '创建附件',
            //       },
            //       {
            //         code: 'ATTACHMENT_UPDATE',
            //         description: '更新附件',
            //       },
            //       {
            //         code: 'ATTACHMENT_DELETE',
            //         description: '删除附件',
            //       },
            //       {
            //         code: 'COMMENT_CREATE',
            //         description: '创建评论',
            //       },
            //       {
            //         code: 'COMMENT_UPDATE',
            //         description: '更新评论',
            //       },
            //       {
            //         code: 'COMMENT_DELETE',
            //         description: '删除评论',
            //       },
            //     ],
            //   },
        },
    })
    const super_admin_user_role = await prisma.userRole.upsert({
        where: { id: 1 },
        update: {},
        create: {
            user_id: super_admin.id,
            role_id: super_admin_role.id,
        },
    })
    await prisma.platform.upsert({
        where: {
            name: '国开'
        },
        create: {
            name: '国开',
            url: 'http://one.ouchn.cn/'
        },
        update: {}
    })
    await prisma.organization.create({
        data: {
            name: '默认代理',
            description: '默认代理',
        }
    })
    console.log({ super_admin, super_admin_role, super_admin_user_role })
}
main()
    .then(async () => {
        await prisma.$disconnect()
    })
    .catch(async (e) => {
        console.error(e)
        await prisma.$disconnect()
        process.exit(1)
    })