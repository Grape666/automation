import { HttpStatus, Injectable } from '@nestjs/common';
import { CreateOrganizationDto } from './dto/create-organization.dto';
import { UpdateOrganizationDto } from './dto/update-organization.dto';
import { PrismaService } from 'src/Pisma/prisma.service';

@Injectable()
export class OrganizationsService {
//   prismaService: any;
  constructor(private readonly prisma: PrismaService) {}

  // 获取机构、平台、学生专业
  async getOrganization() {
    try {
      const organizations = await this.prisma.organization.findMany({
        select: {
          id: true,
          name: true,
        },
      });
      const platforms = await this.prisma.platform.findMany({
        select: {
          id: true,
          name: true,
        },
      });

      return { organizations, platforms };
    } catch (error) {
      console.error('Error occurred while fetching organizations:', error);
      return {
        message: 'Failed to fetch organ izations',
        error: error.message,
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
      };
    }
  }

  //  创建机构
  async create(createOrganizationDto: CreateOrganizationDto) {
    return this.prisma.organization.create({
      data: createOrganizationDto,
    });
  }

  // 查询机构(全部)
  async findAll(query) {    
    const condition = PrismaService.PagingListQueryGenerator(
      this.prisma.studentJob,
      query,
    );
    condition.where = {
      name: {
        contains: query.name,
      },
      description: {
        contains: query.description,
      },
      address: {
        contains: query.address,
      },
      phone: {
        contains: query.phone,
      },
    };
    const [items, total] = await this.prisma.$transaction([
      this.prisma.organization.findMany({...condition,include:{
        _count:{
          select:{
            students:true
          }
        }
      }}),
      this.prisma.organization.count({
        where: condition.where,
      }),
    ]);
    return { items, total };
  }

  findOne(id: number) {
    return `This action returns a #${id} organization`;
  }

  //更新机构,根据id
  async update(id: number, updateOrganizationDto: UpdateOrganizationDto) {
    try {
      const organization = await this.prisma.organization.findUnique({
        where: { id: id },
      });
      if (!organization) {
        return {
          code: 400,
          status: 'error',
          message: '未找到学生记录',
        };
      }
      await this.prisma.organization.update({
        where: { id },
        data: updateOrganizationDto,
      });
      return { code: 200, status: 'success', message: '学生记录更新成功'};
    } catch (error) {
      console.error('更新学生记录时发生错误:', error);
      return { code: 500, status: 'error', message: error };
    }
  }

  //  删除机构,根据id
  async remove(id: number) {
    try {
      const organization = await this.prisma.organization.findUnique({
        where: { id: id },
      });
      if (!organization) {
        return {
          code: 400,
          status: 'error',
          message: '未找到要机构的记录',
        };
      }
      await this.prisma.organization.delete({
        where: { id: id },
      });
      return { code: 200, status: 'success', message: '机构记录删除成功' };
    } catch (error) {
      console.error('机构记录时发生错误:', error);
      return { code: 500, status: 'error', message: error };
    }
  }
}
