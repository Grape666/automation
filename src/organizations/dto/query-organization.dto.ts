import { PagingQueryDto } from 'src/Pisma/dto/query.dto';

export class QueryOrganizationDto extends PagingQueryDto {
  name?: string; //机构名称
  description?: string; //机构描述
  address?: string; //机构地址
  phone?: string; //机构电话
}
  