import { Module } from '@nestjs/common';
import { CourseRunLogService } from './course-run-log.service';
import { CourseRunLogController } from './course-run-log.controller';

@Module({
  controllers: [CourseRunLogController],
  providers: [CourseRunLogService],
})
export class CourseRunLogModule {}
