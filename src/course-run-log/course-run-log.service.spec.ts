import { Test, TestingModule } from '@nestjs/testing';
import { CourseRunLogService } from './course-run-log.service';

describe('CourseRunLogService', () => {
  let service: CourseRunLogService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CourseRunLogService],
    }).compile();

    service = module.get<CourseRunLogService>(CourseRunLogService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
