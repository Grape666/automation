import { Test, TestingModule } from '@nestjs/testing';
import { CourseRunLogController } from './course-run-log.controller';
import { CourseRunLogService } from './course-run-log.service';

describe('CourseRunLogController', () => {
  let controller: CourseRunLogController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CourseRunLogController],
      providers: [CourseRunLogService],
    }).compile();

    controller = module.get<CourseRunLogController>(CourseRunLogController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
