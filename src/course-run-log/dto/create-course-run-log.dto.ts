export class CreateCourseRunLogDto {
    student_run_log_id: number //学生每次挂机记录id
    course_id: number //课程id
    start_time?: Date //开始时间
    end_time?: Date //结束时间
    remark?: string   //备注
    result?: string //结果
}
