import { PartialType } from '@nestjs/swagger';
import { CreateCourseRunLogDto } from './create-course-run-log.dto';

export class UpdateCourseRunLogDto extends PartialType(CreateCourseRunLogDto) {}
