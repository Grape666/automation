import { Injectable } from '@nestjs/common';
import { CreateCourseRunLogDto } from './dto/create-course-run-log.dto';
import { UpdateCourseRunLogDto } from './dto/update-course-run-log.dto';
import { PrismaService } from 'src/Pisma/prisma.service';

@Injectable()
export class CourseRunLogService {
  constructor(private readonly prisma: PrismaService) {}
  create(createCourseRunLogDto: CreateCourseRunLogDto) {
    return  this.prisma.courseRunLog.create({
      data:createCourseRunLogDto
    })
  }

  findAll() {
    return `This action returns all courseRunLog`;
  }

  findOne(id: number) {
    return `This action returns a #${id} courseRunLog`;
  }

  update(id: number, updateCourseRunLogDto: UpdateCourseRunLogDto) {
    return this.prisma.courseRunLog.update({
      where: { id: id },
      data: updateCourseRunLogDto
    });
  }

  remove(id: number) {
    return `This action removes a #${id} courseRunLog`;
  }
}
