import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { CourseRunLogService } from './course-run-log.service';
import { CreateCourseRunLogDto } from './dto/create-course-run-log.dto';
import { UpdateCourseRunLogDto } from './dto/update-course-run-log.dto';

@Controller('course-run-log')
export class CourseRunLogController {
  constructor(private readonly courseRunLogService: CourseRunLogService) {}

  @Post()
  create(@Body() createCourseRunLogDto: CreateCourseRunLogDto) {
    return this.courseRunLogService.create(createCourseRunLogDto);
  }

  @Get()
  findAll() {
    return this.courseRunLogService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.courseRunLogService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCourseRunLogDto: UpdateCourseRunLogDto) {
    return this.courseRunLogService.update(+id, updateCourseRunLogDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.courseRunLogService.remove(+id);
  }
}
