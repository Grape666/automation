// import { Transform } from "class-transformer";
import { IsOptional, IsNumberString, IsBooleanString } from "class-validator";


/**
 * 分页查询参数
 */
export class PagingQueryDto {
  @IsOptional()
  sortBy?: string = "id"

  @IsOptional()
  @IsBooleanString()
  descending?: string = "true"

  @IsOptional()
  @IsNumberString()
  page?: string = "0";

  @IsOptional()
  @IsNumberString()
  per_page?: string = "10";
}

