import {  Injectable, OnModuleInit } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { PagingQueryDto } from './dto/query.dto';

@Injectable()
export class PrismaService extends PrismaClient implements OnModuleInit {
  async onModuleInit() {
    await this.$connect();
  }

  static exclude<T extends { [key: string]: any }, Key extends keyof T>(
    model: T,
    keys: Key[]
  ): Omit<T, Key> {
    return Object.fromEntries(
      Object.entries(model).filter(([key]) => !keys.includes(key as Key))
    ) as Omit<T, Key>;
  }


  /**
   * 分页列表查询生成器
   * 
   * @param fields 字段映射
   * @param query 分页查询参数
   * @param whereMap 额外查询条件函数
   * @returns 分页查询参数对象
   */
  static PagingListQueryGenerator(fields: object, query: PagingQueryDto, whereMap?: () => object): { where: { [key: string]: any }, orderBy: { [key: string]: string }, skip: number, take: number } {
    const keys = Object.keys(fields)
    const where = whereMap ? whereMap() : {};
    const per_page = parseInt(query.per_page ?? "10")
    const page = parseInt(query.page ?? "0")
    const take = per_page;
    const skip = page * per_page
    const sort_by_field = query.sortBy ? keys.find(field => query.sortBy === field) || "id" : "id"
    const descending = query.descending === 'false' ? 'asc' : 'desc'
    return {
      where,
      orderBy: {
        [sort_by_field]: descending
      },
      skip,
      take,
    }
  }
}