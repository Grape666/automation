export class CreateStudentRunLogDto {
    student_id:number;
    user_id:number;
    devicename ?:string; //设备名称
    LAN_IP     ?:string; //本地ip
    ip         ?:string; //自己ip
    start_time ?:Date; //开始时间
    end_time   ?:Date; //结束时间
    remark     ?:string;   //备注
    result     ?:string; //结果
}
