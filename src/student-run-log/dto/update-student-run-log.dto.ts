import { PartialType } from '@nestjs/swagger';
import { CreateStudentRunLogDto } from './create-student-run-log.dto';

export class UpdateStudentRunLogDto extends PartialType(CreateStudentRunLogDto) {}
