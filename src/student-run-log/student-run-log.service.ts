import { Injectable } from '@nestjs/common';
import { CreateStudentRunLogDto } from './dto/create-student-run-log.dto';
import { UpdateStudentRunLogDto } from './dto/update-student-run-log.dto';
import { PrismaService } from 'src/Pisma/prisma.service';

@Injectable()
export class StudentRunLogService {
  constructor(
    private readonly prisma :PrismaService
  ) {}
  create(createStudentRunLogDto: CreateStudentRunLogDto) {
    return this.prisma.studentRunLog.create({
      data: createStudentRunLogDto,
    });
  }

  findAll() {
    return `This action returns all studentRunLog`;
  }

  findOne(id: number) {
    return `This action returns a #${id} studentRunLog`;
  }

  update(id: number, updateStudentRunLogDto: UpdateStudentRunLogDto) {
    return this.prisma.studentRunLog.update({
      where: { id: id },
      data: updateStudentRunLogDto,
    });
  }

  remove(id: number) {
    return `This action removes a #${id} studentRunLog`;
  }
}
