import { Test, TestingModule } from '@nestjs/testing';
import { StudentRunLogService } from './student-run-log.service';

describe('StudentRunLogService', () => {
  let service: StudentRunLogService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StudentRunLogService],
    }).compile();

    service = module.get<StudentRunLogService>(StudentRunLogService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
