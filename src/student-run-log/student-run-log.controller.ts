import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { StudentRunLogService } from './student-run-log.service';
import { CreateStudentRunLogDto } from './dto/create-student-run-log.dto';
import { UpdateStudentRunLogDto } from './dto/update-student-run-log.dto';

@Controller('student-run-log')
export class StudentRunLogController {
  constructor(private readonly studentRunLogService: StudentRunLogService) {}

  @Post()
  create(@Body() createStudentRunLogDto: CreateStudentRunLogDto) {
    return this.studentRunLogService.create(createStudentRunLogDto);
  }

  @Get()
  findAll() {
    return this.studentRunLogService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.studentRunLogService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateStudentRunLogDto: UpdateStudentRunLogDto) {
    return this.studentRunLogService.update(+id, updateStudentRunLogDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.studentRunLogService.remove(+id);
  }
}
