import { Test, TestingModule } from '@nestjs/testing';
import { StudentRunLogController } from './student-run-log.controller';
import { StudentRunLogService } from './student-run-log.service';

describe('StudentRunLogController', () => {
  let controller: StudentRunLogController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StudentRunLogController],
      providers: [StudentRunLogService],
    }).compile();

    controller = module.get<StudentRunLogController>(StudentRunLogController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
