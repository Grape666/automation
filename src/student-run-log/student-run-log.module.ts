import { Module } from '@nestjs/common';
import { StudentRunLogService } from './student-run-log.service';
import { StudentRunLogController } from './student-run-log.controller';

@Module({
  controllers: [StudentRunLogController],
  providers: [StudentRunLogService],
})
export class StudentRunLogModule {}
