import { Module } from '@nestjs/common';
import { StudentJobService } from './student-job.service';
import { StudentJobController } from './student-job.controller';

@Module({
  controllers: [StudentJobController],
  providers: [StudentJobService],
})
export class StudentJobModule {}
