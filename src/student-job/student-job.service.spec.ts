import { Test, TestingModule } from '@nestjs/testing';
import { StudentJobService } from './student-job.service';

describe('StudentJobService', () => {
  let service: StudentJobService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StudentJobService],
    }).compile();

    service = module.get<StudentJobService>(StudentJobService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
