import { Injectable } from '@nestjs/common';
import { CreateStudentJobDto } from './dto/create-student-job.dto';
import { UpdateStudentJobDto } from './dto/update-student-job.dto';
import { PrismaService } from 'src/Pisma/prisma.service';

@Injectable()
export class StudentJobService {
  [x: string]: any;
  constructor(private readonly prisma: PrismaService) { }

  //创建作业
  async create(createStudentJobDto: CreateStudentJobDto) {

    return this.prisma.studentJob.upsert({
      where: {
        unique_student_job_id: {
          student_course_id: createStudentJobDto.student_course_id,
          job_id: createStudentJobDto.job_id,
        }
      },
      create: createStudentJobDto,
      update: {}
    })
  }

  //查询全部
  async findAll(query) {
    const condition = PrismaService.PagingListQueryGenerator(
      this.prisma.studentJob,
      query,
    );
    condition.where = {
      student_course_id: query.student_course_id,
      name: {
        name: query.name,
      },
      job_platform_id: query.job_platform_id,
      syllabus_name: {
        contains: query.syllabus_name,
      },
      chapter_name: {
        condition: query.chapter_name,
      },
      type: {
        condition: query.type,
      },
      parent_id: query.parent_id,
      description: {
        condition: query.description,
      },
      answer_duration: {
        condition: query.answer_duration,
      },
      allow_answer_time: {
        condition: query.allow_answer_time,
      },
      total_score: {
        condition: query.total_score,
      },
      score_percentage: {
        score_percentage: query.score_percentage,
      },
      score_type: {
        condition: query.score_type,
      },
      start_time: {
        condition: query.start_time,
      },
      end_time: {
        condition: query.end_time,
      },
      total_question_num: {
        condition: query.total_question_num,
      },
      question_type: {
        condition: query.question_type,
      },
      answered_time: {
        condition: query.answered_time,
      },
      score: {
        condition: query.score,
      },
      status: {
        condition: query.status,
      },
      remark: {
        condition: query.remark,
      },
    };
    const [items, total] = await this.prisma.$transaction([
      this.prisma.studentJob.findMany(condition),
      this.prisma.studentJob.count({
        where: condition.where,
      }),
    ]);
    return { items, total };
  }

  findOneByStudentCourseIdAndJobId(student_course_id: number, job_id: number) {
    return this.prisma.studentJob.findUnique({
      where: {
        unique_student_job_id: {
          student_course_id, job_id
        }
      },
    });
  }


  async findOne(id: number) {
    try {
      const studentJob = await this.prisma.studentJob.findUnique({
        where: { id: id },
      });
      if (!studentJob) {
        return {
          code: 400,
          status: 'error',
          message: '未找到作业记录',
        };
      }
      return studentJob;
    } catch (error) {
      console.error('查询作业记录时发生错误:', error);
      return { code: 500, status: 'error', message: error };
    }
  }
  // 更新作业，根据id
  async update(id: number, updateStudentJobDto: UpdateStudentJobDto) {
    try {
      const updatestudentJob = await this.prisma.studentJob.update({
        where: { id },
        data: updateStudentJobDto,
      });

      return updatestudentJob;
    } catch (error) {
      console.error('更新作业记录时发生错误:', error);
      return { code: 500, status: 'error', message: error };
    }
  }



  // 删除作业，根据id
  async remove(id: number) {
    try {
      const studentJob = await this.prisma.studentJob.findUnique({
        where: { id: id },
      });
      if (!studentJob) {
        return {
          code: 400,
          status: 'error',
          message: '未找到要删除的作业记录',
        };
      }
      await this.prisma.studentJob.delete({
        where: { id: id },
      });

      return { code: 200, status: 'success', message: '作业记录删除成功' };
    } catch (error) {
      console.error('删除作业记录时发生错误:', error);
      return { code: 500, status: 'error', message: error };
    }
  }
}
