import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { StudentJobService } from './student-job.service';
import { CreateStudentJobDto } from './dto/create-student-job.dto';
import { UpdateStudentJobDto } from './dto/update-student-job.dto';
import { QueryStudentJobDto } from './dto/query-student.dto';

@Controller('studentjob')
export class StudentJobController {
  constructor(private readonly studentJobService: StudentJobService) {}

  @Post()
  async create(@Body() createStudentJobDto: CreateStudentJobDto) {
    return this.studentJobService.create(createStudentJobDto);
  }
  
  @Get()
  findAll(@Query() query:QueryStudentJobDto){
    return this.studentJobService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.studentJobService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateStudentJobDto: UpdateStudentJobDto) {
    return this.studentJobService.update(+id, updateStudentJobDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {

    return { code: 500, status: 'error', message: id + '作业禁止删除' };
    // return this.studentJobService.remove(+id);
  }
}
