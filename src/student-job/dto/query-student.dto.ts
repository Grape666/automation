import { PagingQueryDto } from 'src/Pisma/dto/query.dto';

export class QueryStudentJobDto extends PagingQueryDto {
  
  student_course_id: number; //学生课程id
  name: string; //作业名称
  job_platform_id?: string; // 平台的作业id
  syllabus_name?: string; //大纲名称
  chapter_name?: string; //章节名称
  type: string; //学生作业类型
  parent_id?: number; //父级作业id 复合型作业
  description?: string; //作业描述
  answer_duration?: string; //答题时间
  allow_answer_time?: string; //允许做答次数
  total_score?: string; //学生作业总分
  score_percentage?: string; //学生作业成绩占比
  score_type?: string; //评分方式 最高分/最后得分 根据平台定
  start_time?: Date; //在平台中的作业开始时间
  end_time?: Date; //在平台中的作业结束时间
  total_question_num?: string; //题目总数量
  question_type?: string; //题目类型
  answered_time?: string; //已经作答次数
  score?: string; //学生作业成绩
  status?: string; //作业状态 未开始/进行中/已完成/已批改/已提交/已评阅/已取消
  remark?: string; //作业备注
}
