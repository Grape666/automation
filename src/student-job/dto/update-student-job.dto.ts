import { PartialType } from '@nestjs/swagger';
import { CreateStudentJobDto } from './create-student-job.dto';

export class UpdateStudentJobDto extends PartialType(CreateStudentJobDto) {}
