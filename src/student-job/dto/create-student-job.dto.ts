export class CreateStudentJobDto {
  student_course_id: number; //学生课程id
  job_id: number; //作业id
  answered_time?: number //已经作答次数
  score?: number //学生作业成绩
  status?: string //作业状态 未开始/进行中/已完成/已批改/已提交/已评阅/已取消
  remark?: string //作业备注
}
