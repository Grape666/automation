import { Test, TestingModule } from '@nestjs/testing';
import { StudentJobController } from './student-job.controller';
import { StudentJobService } from './student-job.service';

describe('StudentJobController', () => {
  let controller: StudentJobController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StudentJobController],
      providers: [StudentJobService],
    }).compile();

    controller = module.get<StudentJobController>(StudentJobController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
