import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { StudentsModule } from './students/students.module';
import { PrismaModule } from './Pisma/prisma.module';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { StudentCoursesModule } from './student-courses/student-courses.module';
import { StudentJobModule } from './student-job/student-job.module';
import { PlatformsModule } from './platforms/platforms.module';
import { ReviewModule } from './review/review.module';
import { RolesModule } from './roles/roles.module';
import { OrganizationsModule } from './organizations/organizations.module';
import { PermissionsModule } from './permissions/permissions.module';
import { StudentMessageModule } from './student-message/student-message.module';
import { CourseDisposeLogModule } from './course-dispose-log/course-dispose-log.module';
import { PlatformManageModule } from './platform-manage/platform-manage.module';
import { OrganizationUserModule } from './organization-user/organization-user.module';
import { RobotModule } from './robot/robot.module';
import { CoursesModule } from './courses/courses.module';
import { JobsModule } from './jobs/jobs.module';
import { CourseRunLogModule } from './course-run-log/course-run-log.module';
import { StudentRunLogModule } from './student-run-log/student-run-log.module';
import { ReviewStudentAccountModule } from './review-student-account/review-student-account.module';
@Module({
  imports: [PrismaModule, StudentsModule, AuthModule, UsersModule, StudentCoursesModule, StudentJobModule, PlatformsModule, ReviewModule, RolesModule, OrganizationsModule, PermissionsModule, StudentMessageModule, CourseDisposeLogModule, PlatformManageModule, OrganizationUserModule, RobotModule, CoursesModule, JobsModule, CourseRunLogModule, StudentRunLogModule, ReviewStudentAccountModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
