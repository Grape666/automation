import { Test, TestingModule } from '@nestjs/testing';
import { PlatformManageService } from './platform-manage.service';

describe('PlatformManageService', () => {
  let service: PlatformManageService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PlatformManageService],
    }).compile();

    service = module.get<PlatformManageService>(PlatformManageService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
