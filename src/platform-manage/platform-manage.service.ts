import { Injectable } from '@nestjs/common';
import { CreatePlatformManageDto } from './dto/create-platform-manage.dto';
import { UpdatePlatformManageDto } from './dto/update-platform-manage.dto';
import { PrismaService } from 'src/Pisma/prisma.service';
import { QueryPlatformManageDto } from './dto/query-platform-manage.dto';

@Injectable()
export class PlatformManageService {
  constructor(private readonly prisma: PrismaService) {}

  // 创建平台后台管理账号
  create(createPlatformManageDto: CreatePlatformManageDto) {
    createPlatformManageDto.platform_id = Number(
      createPlatformManageDto.platform_id,
    );
    createPlatformManageDto.organization_id = Number(
      createPlatformManageDto.organization_id,
    );
    return this.prisma.platformManageAccount.create({
      data: createPlatformManageDto,
    });
  }

  // 查询平台后台管理账号,分页
  async findAll(query: QueryPlatformManageDto) {
    const condition = PrismaService.PagingListQueryGenerator(
      this.prisma.platformManageAccount,
      query,
    );
    condition.where = {
      name: {
        contains: query.name,
      },
      account: {
        contains: query.account,
      },
      platform_id: query.platform_id && +query.platform_id,
      organization_id: query.organization_id && +query.organization_id,
      learning_center: {
        condition: query.learning_center,
      },
    };

    const [items, total] = await this.prisma.$transaction([
      this.prisma.platformManageAccount.findMany(condition),
      this.prisma.platformManageAccount.count({
        where: condition.where,
      }),
    ]);
    return { items, total };
  }

  findOne(id: number) {
    return `This action returns a #${id} platformManage`;
  } 

  // 更新平台后台管理账号，根据id
  async update(id: number, updatePlatformManageDto: UpdatePlatformManageDto) {
    try {
      const platformManageAccount =
        await this.prisma.platformManageAccount.findUnique({
          where: { id: id },
        });
      if (!platformManageAccount) {
        return {
          code: 200,
          status: 'error',
          message: '未找到平台后台管理账号记录',
        };
      }
        await this.prisma.platformManageAccount.update({
          where: { id: id },
          data: updatePlatformManageDto,
        });
      return {
        code: 200,
        status: 'success',
        message: '平台后台管理账号更新成功',
      };
    } catch (error) {
      console.error('更新平台后台管理账号时发生错误:', error);
      return { code: 500, status: 'error', message: error };
    }
  }
  //删除平台后台管理账号：根据id
  async remove(id: number) {
    try {
      const platformManageAccount = await this.prisma.platformManageAccount.findUnique({
        where: { id: id },
      });
      if (!platformManageAccount) {
        return {
          code: 400,
          status: 'error',
          message: '未找到要删除的平台后台管理账号记录',
        };
      }
      await this.prisma.platformManageAccount.delete({
        where: { id: id },
      });
      return { code: 200, status: 'success', message: '删除平台后台管理账号记录删除成功' };
    } catch (error) {
      console.error('删除平台后台管理账号记录时发生错误:', error);
      return { code: 500, status: 'error', message: error };
    }
  }
}
