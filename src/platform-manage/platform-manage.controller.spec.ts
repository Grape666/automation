import { Test, TestingModule } from '@nestjs/testing';
import { PlatformManageController } from './platform-manage.controller';
import { PlatformManageService } from './platform-manage.service';

describe('PlatformManageController', () => {
  let controller: PlatformManageController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PlatformManageController],
      providers: [PlatformManageService],
    }).compile();

    controller = module.get<PlatformManageController>(PlatformManageController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
