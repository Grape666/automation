import { PartialType } from '@nestjs/swagger';
import { CreatePlatformManageDto } from './create-platform-manage.dto';

export class UpdatePlatformManageDto extends PartialType(CreatePlatformManageDto) {}
