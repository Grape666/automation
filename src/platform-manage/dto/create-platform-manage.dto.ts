export class CreatePlatformManageDto {
  name?: string; //姓名
  account: string; //账号
  password: string; //密码
  platform_id: number; //平台 id
  organization_id: number; //机构 id
  learning_center: string; //账号所属学习中心
}
