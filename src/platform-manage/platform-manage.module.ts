import { Module } from '@nestjs/common';
import { PlatformManageService } from './platform-manage.service';
import { PlatformManageController } from './platform-manage.controller';

@Module({
  controllers: [PlatformManageController],
  providers: [PlatformManageService],
})
export class PlatformManageModule {}
