import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { PlatformManageService } from './platform-manage.service';
import { CreatePlatformManageDto } from './dto/create-platform-manage.dto';
import { UpdatePlatformManageDto } from './dto/update-platform-manage.dto';
import { QueryPlatformManageDto } from './dto/query-platform-manage.dto';

@Controller('platform-manage')
export class PlatformManageController {
  constructor(private readonly platformManageService: PlatformManageService) {}

  @Post()
  create(@Body() createPlatformManageDto: CreatePlatformManageDto) {
    return this.platformManageService.create(createPlatformManageDto);
  }

  @Get()
  findAll(@Query() query:QueryPlatformManageDto) {
    return this.platformManageService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.platformManageService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updatePlatformManageDto: UpdatePlatformManageDto) {
    return this.platformManageService.update(+id, updatePlatformManageDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return { code: 500, status: 'error', message: '删除平台后台管理账号禁止删除' };
    // return this.platformManageService.remove(+id);
  }
}
