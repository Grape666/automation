import { PagingQueryDto } from 'src/Pisma/dto/query.dto';

export class QueryReviewDto extends PagingQueryDto {
    name:string //老师账号的名称
    account:string  //老师评阅账号
    student_id:number //学生id
    student_course_id:number //学生课程id
    student_job_id:number //学生作业id
    score:string //评阅成绩
}
