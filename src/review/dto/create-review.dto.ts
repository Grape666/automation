export class CreateReviewDto {
  name?: string; //学生姓名
  school_name?: string; // 学校名称
  student_account: string;//学生账号
  platform_course_id: string;//平台课程id
  platform_job_id: string;// 平台的作业id
  course_id: number;//课程id
  job_id: number;//作业id
  review_student_account_id?: number; //评阅学生账号id
  student_id?: number; //学生id
  student_course_id?: number; //学生课程id
  student_job_id?: number; //学生作业id
  status: string;
  score?: number; //评阅成绩
  startDate?:Date; //评阅记录开始日期
  endDate?:Date; //评阅记录结束日期
}