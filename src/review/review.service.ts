import { Injectable } from '@nestjs/common';
import { CreateReviewDto } from './dto/create-review.dto';
import { UpdateReviewDto } from './dto/update-review.dto';
import { PrismaService } from 'src/Pisma/prisma.service';

import {stream} from 'exceljs';
import { Response } from 'express';

@Injectable()
export class ReviewService {
  constructor(private readonly prisma: PrismaService) {}

  // 创建评阅
  async create(createReviewDto: CreateReviewDto) {
    createReviewDto.student_id =
      createReviewDto.student_id && Number(createReviewDto.student_id);
    createReviewDto.student_course_id =
      createReviewDto.student_course_id &&
      Number(createReviewDto.student_course_id);
    createReviewDto.student_job_id =
      createReviewDto.student_job_id && Number(createReviewDto.student_job_id);

    return this.prisma.reviewRecord.create({
      data: createReviewDto,
    });
  }

  //查询全部评阅记录
  async findAll(query) {
    const condition = PrismaService.PagingListQueryGenerator(
      this.prisma.reviewRecord,
      query,
    );
    condition.where = {
      name: {
        contains: query.name,
      },
      student_account: {
        contains: query.student_account,
      },
      platform_course_id: query.platform_course_id,
      platform_job_id: query.platform_job_id,
      course_id: query.course_id && Number(query.course_id),
      job_id: query.job_id && Number(query.job_id),
      review_student_account_id:
        query.review_student_account_id &&
        Number(query.review_student_account_id),
      student_id: query.student_id && Number(query.student_id),
      student_course_id:
        query.student_course_id && Number(query.student_course_id),
      student_job_id: query.student_job_id && Number(query.student_job_id),
      score: query.score,
    };

    const [items, total] = await this.prisma.$transaction([
      this.prisma.reviewRecord.findMany({
        ...condition,
        include: {
          course: true,
          job: true,
          review_student_account: true,
          student: true,
          student_course: true,
          student_job: true,
        },
      }),
      this.prisma.reviewRecord.count({
        where: condition.where,
      }),
    ]);
    return { items, total };
  }

  findOne(id: number) {
    return `This action returns a #${id} organization`;
  }

  //更新评阅，根据id
  async update(id: number, updateReviewDto: UpdateReviewDto) {
    try {
      const reviewRecord = await this.prisma.reviewRecord.findUnique({
        where: { id: id },
      });
      if (!reviewRecord) {
        return {
          code: 400,
          status: 'error',
          message: '未找到评阅记录',
        };
      }
      await this.prisma.reviewRecord.update({
        where: { id },
        data: updateReviewDto,
      });
      return { code: 200, status: 'success', message: '评阅记录更新成功' };
    } catch (error) {
      console.error('更新评阅记录时发生错误:', error);
      return { code: 500, status: 'error', message: error };
    }
  }

  remove(id: number) {
    return `This action removes a #${id} review`;
  }
  
  //查询学生的评阅结果
  async reviewFail(res:Response,lowest_mark:number) {
    const date = new Date(); // 获取当前日期
    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    res.setHeader('Content-Disposition', `attachment; filename=${encodeURIComponent('评阅结果' + "-" + date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate())}.xlsx`);

    const workbook = new stream.xlsx.WorkbookWriter({
      stream:res
    });
    const worksheet = workbook.addWorksheet('评阅结果');
    worksheet.columns = [
      { header: '学生姓名', key: 'student_name' },
      { header: '学生账号', key:'student_account' },
      { header: '密码', key: 'password' },
      { header: '专业', key:'major' },
      { header: '登录状态', key: 'login_status' },
      { header: '登录备注', key:'remark' },
      { header: '课程名称', key: 'course_name' },
      { header: '形考成绩', key: 'formal_scores' },
      { header: '形考总分', key: 'formal_total_score' },
      { header: '终考成绩', key: 'final_score' },
      { header: '终考总分', key: 'final_total_score' },
      { header: '总分', key: 'totol_score' },
      { header: '课程进度', key:'schedule' },
      { header: '平台课程ID', key: 'platform_course_id' },
      { header: '作业名称', key: 'job_name' },
      { header: '作业平台ID', key: 'job_platform_id' },
      { header: '作业章节', key: 'job_syllabus_name' },
      { header: '作业大纲', key: 'job_chapter_name' },
      { header: '作业类型', key: 'job_type' },
      { header: '作业分数', key: 'job_score' },
      { header: '作业状态', key: 'job_status' },
      { header: '作业备注', key: 'job_remark' },
      { header: '作业分数类型', key: 'job_score_type' },
      { header: '评阅最高分', key: 'review_record_max_score' },
      { header: '评阅记录数量', key: 'review_record_count' },
    ];
    const student_total = await this.prisma.student.count();
    const per_count = 100;
    const total_page_num = Math.ceil(student_total / per_count);
    
    for (let i = 0; i < total_page_num; i++) {
      // 从获取到的学生数据中提取出账号信息
      const sutdents = await this.prisma.student.findMany({
        skip: i * per_count,
        take: per_count,
        include: {
          courses: {
            include: {
              course: true,
              jobs: {
                include: {
                  job: true,
                  ReviewRecord: true,
                },
              },
            },
          },
        },
      });
      // 从获取到的学生数据中提取出账号信息
      const account_ids = sutdents.map((item) => item.account);
      // 根据提取的账号信息
      const review_record_accounts =
        await this.prisma.reviewStudentAccount.findMany({
          where: {
            account: {
              in: account_ids,
            },
          },
        });
      const review_student = sutdents.flatMap((item) => {
        // 在查找到的评阅账号记录中，查找与当前学生账号匹配的记录
        const review_record_account = review_record_accounts.find(
          (review_record_account) =>
            review_record_account.account === item.account,
        );

        // 如果找到了匹配的评阅账号记录
        if (review_record_account) {
          return [item];
        } else {
          return [];
        }
      });
      for (let i = 0; i < review_student.length; i++) {
        const student = review_student[i];
        const courses = student.courses;
        for (let j = 0; j < courses.length; j++) {
          const course = courses[j];
          const jobs = course.jobs;
          for (let k = 0; k < jobs.length; k++) {
            const job = jobs[k];
            if (!job.score || job.score < lowest_mark) {
              const review_record = jobs[k].ReviewRecord;
              let need_check = false;
              let review_record_max_score = undefined;
              if (review_record.length > 0) {
                // 有评阅记录
                for (let l = 0; l < review_record.length; l++) {
                  const review = review_record[l];
                  // 找到最高分
                  if (
                    review.score &&
                    (review_record_max_score === undefined || review.score > review_record_max_score)
                  ) {
                    review_record_max_score = review.score;
                  }
                }
                if (!review_record_max_score || review_record_max_score < lowest_mark) {
                  need_check = true;
                }
              } else {
                //  没有评阅记录
                need_check = true;
              }
              if (need_check) {
                
                const data = {
                  student_name: student.name,
                  student_account: student.account,
                  password: student.password,
                  major: student.major,
                  login_status: student.login_status,
                  remark: student.remark,
                  course_name: course.course.name,
                  formal_scores: course.formal_scores,
                  formal_total_score: course.formal_total_score,
                  final_score: course.final_score,
                  final_total_score: course.final_total_score,
                  totol_score: course.totol_score,
                  schedule: course.schedule,
                  platform_course_id: course.course.platform_course_id,
                  job_name: job.job.name,
                  job_platform_id:job.job.job_platform_id,
                  job_syllabus_name: job.job.syllabus_name,
                  job_chapter_name: job.job.chapter_name,
                  job_type: job.job.type,
                  job_score: job.score,
                  job_status: job.status,
                  job_remark: job.remark,
                  job_score_type: job.job.score_type,
                  review_record_max_score: review_record_max_score,
                  review_record_count: review_record.length
                }
                worksheet.addRow(data).commit();
              }
            }
          }
        }
      }
    }



    await workbook.commit();
  }

  async reviewResult( res:Response) {
    const date = new Date(); // 获取当前日期
    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    res.setHeader('Content-Disposition', `attachment; filename=${encodeURIComponent('评阅记录' + "-" + date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate())}.xlsx`);

    const workbook = new stream.xlsx.WorkbookWriter({
      stream:res
    });
    const worksheet = workbook.addWorksheet('评阅记录');
    worksheet.columns = [
      { header: 'ID', key: 'id' },
      { header: '学生姓名', key:'student_name' },
      { header: '学校名称', key:'school_name' },
      { header: '学生账号', key:'student_account' },
      { header: '密码', key:'password' },
      // { header: '平台课程ID', key: 'platform_course_id' },
      { header: '课程名称', key: 'course_name' },
      // { header: '平台作业ID', key: 'platform_job_id' },
      { header: '作业章节', key: 'job_syllabus_name' },
      { header: '作业大纲', key: 'job_chapter_name' },
      { header: '作业名称', key: 'job_name' },
      { header: '作业类型', key: 'job_type' },
      { header: '作业描述', key: 'job_description' },
      { header: '题目类型', key: 'job_question_type' },
      { header: '是否是评阅学生账号', key: 'is_review_student_account' },
      { header: '是否是挂课学生账号', key: 'is_student' },
      { header: '状态', key:'status' },
      { header: '分数', key:'score' },
      { header: '评阅时间', key:'review_time' },
    ];
    const review_record_count = await this.prisma.reviewRecord.count();
    const per_count = 1000;
    for(let i = 0; i < Math.ceil(review_record_count/per_count); i++){
      const review_records = await this.prisma.reviewRecord.findMany({
        skip: i*per_count,
        take: per_count,
        orderBy:{
            created_at: 'desc'
        },
        include:{
          course:true,
          job:true,
          review_student_account:true,
          student:true,
          student_course:true,
          student_job:true,
        }
      })
      const data = review_records.map((item) => {
        return {
          id: item.id,
          student_name:item.name,
          school_name:item.school_name,
          student_account:item.student_account,
          password:item.student?.password,
          // platform_course_id : item.platform_course_id, //平台课程id
          course_name:item.course.name,
          // platform_job_id : item.platform_job_id, // 平台的作业id
          job_syllabus_name:item.job.syllabus_name,
          job_chapter_name:item.job.chapter_name,
          job_name:item.job.name,
          job_type:item.job.type,
          job_question_type:item.job.question_type,
          job_description:item.job.description,
          is_review_student_account: item.review_student_account_id? "是":"否",
          is_student:item.student_id ? "是":"否",
          status:item.status,
          score:item.score,
          review_time:item.created_at,
        }
      })
      data.forEach((item) => {
        worksheet.addRow(item).commit();
      })
    }
    await workbook.commit();
  }
  async reviewLastResult( start: string, end: string, res:Response) {
    start = '2024-06-26 00:00:00';
    end = '2024-06-26 00:10:00';
    const date = new Date(); // 获取当前日期
    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    const randomNumber = Math.floor(Math.random() * 10000); // 生成一个 0 到 9999 之间的随机整数
    res.setHeader('Content-Disposition', `attachment; filename=${encodeURIComponent('评阅记录' + "-" + date.getFullYear() 
    + "-" + (date.getMonth() + 1) + "-" + date.getDate() + "-" + randomNumber)}.xlsx`);
    const workbook = new stream.xlsx.WorkbookWriter({
      stream:res
    });
    const worksheet = workbook.addWorksheet('评阅记录');
    worksheet.columns = [
      { header: 'ID', key: 'id' },
      { header: '学生姓名', key:'student_name' },
      { header: '学校名称', key:'school_name' },
      { header: '学生账号', key:'student_account' },
      { header: '平台课程ID', key: 'platform_course_id' },
      { header: '课程名称', key: 'course_name' },
      { header: '平台作业ID', key: 'platform_job_id' },
      { header: '作业章节', key: 'job_syllabus_name' },
      { header: '作业大纲', key: 'job_chapter_name' },
      { header: '作业名称', key: 'job_name' },
      { header: '作业类型', key: 'job_type' },
      { header: '是否是评阅学生账号', key: 'is_review_student_account' },
      { header: '是否是挂课学生账号', key: 'is_student' },
      { header: '状态', key:'status' },
      { header: '分数', key:'score' },
      { header: '评阅时间', key:'review_time' },
    ];
    const review_record_count = await this.prisma.reviewRecord.count({
      where: {
        created_at: {
          gte: new Date(start),
          lte: new Date(end),
        },
      },
    });
    // const review_record_count = 3000;
    const per_count = 1000;
    for(let i = 0; i < Math.ceil(review_record_count/per_count); i++){
      const review_records = await this.prisma.reviewRecord.findMany({
        skip: i*per_count,
        take: per_count,
        where: {
          created_at: {
            gte: new Date(start),
            lte: new Date(end),
          },
        },
        include:{
          course:true,
          job:true,
          review_student_account:true,
          student:true,
          student_course:true,
          student_job:true,
        }
      })
      const data = review_records.map((item) => {
        return {
          id: item.id,
          student_name:item.name,
          school_name:item.school_name,
          student_account:item.student_account,
          platform_course_id : item.platform_course_id, //平台课程id
          course_name:item.course.name,
          platform_job_id : item.platform_job_id, // 平台的作业id
          job_syllabus_name:item.job.syllabus_name,
          job_chapter_name:item.job.chapter_name,
          job_name:item.job.name,
          job_type:item.job.type,
          is_review_student_account: item.review_student_account_id? "是":"否",
          is_student:item.student_id ? "是":"否",
          status:item.status,
          score:item.score,
          review_time:item.created_at,
        }
      })
      data.forEach((item) => {
        worksheet.addRow(item).commit();
      })
    }
    await workbook.commit();
  }
}
