import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  ParseFloatPipe,
  Res,
} from '@nestjs/common';
import { ReviewService } from './review.service';
import { CreateReviewDto } from './dto/create-review.dto';
import { UpdateReviewDto } from './dto/update-review.dto';
import {  QueryReviewDto } from './dto/query-review.dto';
import { Response } from 'express';

@Controller('review')
export class ReviewController {
  constructor(private readonly reviewService: ReviewService) {}

  @Post()
  create(@Body() createReviewDto: CreateReviewDto) {
    return this.reviewService.create(createReviewDto);
  }

  @Get()
  findAll(@Query() query: QueryReviewDto) {
    return this.reviewService.findAll(query);
  }

  @Get('fail')
  reviewFail(@Res() res:Response, @Query("lowest_mark",ParseFloatPipe) lowest_mark: number) {
    return this.reviewService.reviewFail(res,lowest_mark); 
  }

  @Get('result')
  reviewResult(@Res() res:Response) {
    return this.reviewService.reviewResult(res); 
  }
  @Get('lastResult')
  reviewLastResult(@Param('s') start: string ,@Param('e') end: string ,@Res() res:Response) {
    return this.reviewService.reviewLastResult(start,end,res); 
  }
  // @Get('result')
  // async getReviewResult() {
  //   const reviewResult = await this.reviewService.reviewResult();
  //   return reviewResult;
  // }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.reviewService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateReviewDto: UpdateReviewDto) {
    return this.reviewService.update(+id, updateReviewDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.reviewService.remove(+id);
  }
}
