export class CreateStudentDto {
  name?: string;
  account: string;
  password: string;
  organization_id: number;
  platform_id: number; //平台id
  major?: string; //专业
  remark?: string; // 备注: 登录失败原因
  study_season:string; //学期
  // season: string; //学期

  finished_status?: boolean; // 完成状态：true 已完成,  false 未完成
  login_status?: string; // 登录状态:未登录/登录成功/登录失败
  calculate_status?: boolean; // 计算状态 false 未计算/ true 计算完成
  run_status?: string; // 运行状态 ： running 运行中 pending 等待运行
  run_count?: number; // 运行次数  0 次代表未运行
}

export class ImportStudentDto {
  name?: string;
  account: string;
  password: string;
  organization_id: number;
  platform_id: number; //平台
  major?: string; //专业
  remark?: string; // 备注
  subjects: string[]; //学科
  study_season:string; //学期
  // season: string; //学期
}
