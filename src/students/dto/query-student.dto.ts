import { PagingQueryDto } from 'src/Pisma/dto/query.dto';

export class QueryStudentDto extends PagingQueryDto {
  name?: string;
  platform_id?: string;
  organization_id?: string;
  account?: string;
  major?: string;
  study_season:string
  remark?: string;
  finished_status?: string;
  run_count?:string;
  run_status?:string;
  calculate_status?:string;
  login_status?:string;
  created_at?:Date;
}
