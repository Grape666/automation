import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateStudentDto, ImportStudentDto } from './dto/create-student.dto';
import { UpdateStudentDto } from './dto/update-student.dto';
import { PrismaService } from 'src/Pisma/prisma.service';
import { QueryStudentDto } from './dto/query-student.dto';
import { isBoolean } from 'class-validator';

@Injectable()
export class StudentsService {
  constructor(private readonly prisma: PrismaService) {}

  async create(createStudentDto: CreateStudentDto) {
    try {
      //   查询用户名是否存在，存在报错
      const student = await this.prisma.student.findUnique({
        where: {
          unique_account_platform: {
            account: createStudentDto.account,
            platform_id: createStudentDto.platform_id,
          },
        },
      });
      if (student) {
        return {
          code: 400,
          status: 'error',
          message: '学生账号已存在！',
        };
      }

      // 不存在则创建
      const data = await this.prisma.student.create({
        data:{
          ...createStudentDto,
        }
      });
      return { code: 200, status: 'success', message: '创建成功', data: data };
    } catch (error) {
      //   throw new console.error('创建学生记录时发生错误:', error);
      return { code: 500, status: 'error', message: error };
    }
  }

  /**
   * 根据账号和平台确定唯一键，创建或更新学生信息
   *
   * 可以更新除了账号、平台外的其他信息
   * @param account 学生账号
   * @param createStudentDto 学生信息
   * @returns 学生信息
   */
  createOrUpdateByAccount(account: string, createStudentDto: CreateStudentDto) {
    return this.prisma.$transaction(async (prisma) => {
      return await prisma.student.upsert({
        where: {
          unique_account_platform: {
            account,
            platform_id: createStudentDto.platform_id,
          },
        },
        create: createStudentDto,
        update: {
          name: createStudentDto.name || undefined,
          password: createStudentDto.password || undefined,
          major: createStudentDto.major || undefined,
          study_season: createStudentDto.study_season || undefined,
          remark: createStudentDto.remark || undefined,
          login_status: createStudentDto.login_status || undefined,
          calculate_status:
            (isBoolean(createStudentDto.calculate_status) &&
              createStudentDto.calculate_status) ||
            undefined,
          run_status: createStudentDto.run_status || undefined,
          run_count: {
            increment: createStudentDto.run_count || 0,
          },
          finished_status:
            (isBoolean(createStudentDto.finished_status) &&
              createStudentDto.finished_status) ||
            undefined,
        },
      });
    });
  }
  /**
   * 根据账号和平台确定唯一键，更新学生信息
   * @param account 学生账号
   * @param updateStudentDto 更新的学生信息
   * @returns 学生信息
   */
  updateByAccount(updateStudentDto: UpdateStudentDto) {
    return this.prisma.student
      .update({
        where: {
          unique_account_platform: {
            account: updateStudentDto.account,
            platform_id: updateStudentDto.platform_id,
          },
        },
        data: {
          name: updateStudentDto.name || undefined,
          password: updateStudentDto.password || undefined,
          major: updateStudentDto.major || undefined,
          remark: updateStudentDto.remark || undefined,
          login_status: updateStudentDto.login_status || undefined,
          calculate_status: updateStudentDto.calculate_status || undefined,
          run_status: updateStudentDto.run_status || undefined,
          // study_season: updateStudentDto.study_season || undefined,
          run_count: {
            increment: updateStudentDto.run_count || 0,
          },
          finished_status: updateStudentDto.finished_status || undefined,
        },
      })
      .catch((e) => {
        console.error(e);

        throw new BadRequestException('学生账号不存在');
      });
  }
  //查询所有列表，分页
  async findAll(query: QueryStudentDto) {
    const condition = PrismaService.PagingListQueryGenerator(
      this.prisma.student,
      query,
    );
    condition.where = {
      platform_id: query.platform_id && +query.platform_id,
      organization_id: query.organization_id && +query.organization_id,
      major: {
        contains: query.major,
      },
      remark: {
        contains: query.remark,
      },
      finished_status:
        query.finished_status && query.finished_status === 'true',
      run_count: query.run_count && parseInt(query.run_count),
      run_status: query.run_status,
      login_status: query.login_status,
      calculate_status:
        query.calculate_status && query.calculate_status === 'true',
    };

    if (query.name || query.account) {
      condition.where.OR = [
        {
          name: {
            contains: query.name,
          },
        },
        {
          account: {
            contains: query.account,
          },
        },
      ];
    }
    const [items, total] = await this.prisma.$transaction([
      this.prisma.student.findMany({
        ...condition,
        include: {
          platforms: {
            select: {
              id: true,
              name: true,
            },
          },
          organizations: {
            select: {
              id: true,
              name: true,
            },
          },
          reviews: true,
          _count: true,
          courses: {
            include: {
              jobs: true,
            },
          },
          student_messages: true,
          student_run_logs: true,
        },
      }),
      this.prisma.student.count({
        where: condition.where,
      }),
    ]);
    return { items, total };
  }

  // 查询单个学生
  async findOne(id: number) {
    try {
      const student = await this.prisma.student.findUnique({
        where: { id: id },
      });
      if (!student) {
        return {
          code: 400,
          status: 'error',
          message: '未找到学生记录',
        };
      }
      return student;
    } catch (error) {
      console.error('查询学生记录时发生错误:', error);
      return { code: 500, status: 'error', message: error };
    }
  }

  //更新学生信息
  async update(id: number, updateStudentDto: UpdateStudentDto) {
    try {
      const student = await this.prisma.student.findUnique({
        where: { id: id },
      });

      if (!student) {
        return {
          code: 200,
          status: 'error',
          message: '未找到学生记录',
        };
      }
      await this.prisma.student.update({
        where: { id },
        data: updateStudentDto,
      });

      return { code: 200, status: 'success', message: '学生记录更新成功' };
    } catch (error) {
      console.error('更新学生记录时发生错误:', error);
      return { code: 500, status: 'error', message: error };
    }
  }

  //删除学生：根据id
  async remove(id: number) {
    try {
      const student = await this.prisma.student.findUnique({
        where: { id: id },
      });
      if (!student) {
        return {
          code: 400,
          status: 'error',
          message: '未找到要删除的学生记录',
        };
      }
      await this.prisma.student.delete({
        where: { id: id },
      });

      return { code: 200, status: 'success', message: '学生记录删除成功' };
    } catch (error) {
      console.error('删除学生记录时发生错误:', error);
      return { code: 500, status: 'error', message: error };
    }
  }
  //导入学生
  import(students: Array<ImportStudentDto>) {
    return this.prisma.$transaction((prisma) => {
      return Promise.all(
        students.map(async (student) => {
          // 必要字段是否符合格式
          if (
            typeof student.platform_id !== 'number' ||
            typeof student.organization_id !== 'number'
          ) {
            return {
              code: 401,
              status: 'error',
              message: '机构或者平台格式不符合要求',
              data: null,
            };
          }
          try {
            // 查询数据库中是否已存在同名同账户的学生
            const existingStudent = await prisma.student.findUnique({
              where: {
                unique_account_platform: {
                  account: student.account,
                  platform_id: student.platform_id,
                },
              },
            });
            // 如果学生不存在，则创建新记录
            if (!existingStudent) {
              await prisma.student.create({
                data: {
                  name: student.name,
                  account: student.account.toString().trim(),
                  password: student.password.toString(),
                  platform_id: student.platform_id,
                  organization_id: student.organization_id,
                  major: student.major,
                  study_season:student.study_season,
                  remark: student.remark,
                },
              });
              return {
                code: 200,
                status: 'success',
                message: '导入成功了',
                data: { items: { ...existingStudent } },
              };
            } else {
              return {
                code: 400,
                status: 'error',
                message: '学生账号重复已存在',
                data: { items: { ...existingStudent } },
              };
            }
          } catch (error) {
            const errorField = error.meta.target;
            console.error('导入记录时发生错误:', error);
            return {
              code: 500,
              status: 'error',
              message: `导入格式错误',出错字段: ${errorField}`,
              data: null,
            };
          }
        }),
      );
    });
  }

  //根据账号和平台确定唯一键，查询一个学生信息
  findByAccount(account: string, platform_id: number) {
    return this.prisma.student.findUnique({
      where: {
        unique_account_platform: {
          account,
          platform_id,
        },
      },
    });
  }

  findManyByAccounts(accounts: Array<string>) {
    return this.prisma.student.findMany({
      where: {
        account: {
          in: accounts,
        },
      },
    });
  }
}
