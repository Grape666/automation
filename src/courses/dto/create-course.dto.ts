export class CreateCourseDto {
    platform_course_id:string;
    name:string;
    platform_id:number;
    course_url?:string;
    start_time?:Date;
    end_time?:Date;
}
