import { Injectable } from '@nestjs/common';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { PrismaService } from 'src/Pisma/prisma.service';

@Injectable()
export class CoursesService {
  constructor(private readonly prisma: PrismaService) {}
  create(createCourseDto: CreateCourseDto) {
    return this.prisma.course.upsert({
      where: {
        unique_platform_course: {
          platform_id: createCourseDto.platform_id,
          platform_course_id: createCourseDto.platform_course_id,
        },
      },
      create: createCourseDto,
      update: {},
    });
  }

  findAll() {
    return `This action returns all courses`;
  }

  findOne(id: number) {
    return `This action returns a #${id} course`;
  }
  findOneByPlatformCourseId(platform_course_id: string, platform_id: number) {
    return this.prisma.course.findUnique({
      where: {
        unique_platform_course: {
          platform_id,
          platform_course_id,
        },
      },
    });
  }

  // 更新课程记录，根据id
  async update(id: number, updateCourseDto: UpdateCourseDto) {
    try {
      const course = this.prisma.course.findUnique({
        where: { id: id },
      });
      if (!course) {
        return {
          code: 400,
          status: 'error',
          message: '未找到课程记录',
        };
      }
      await this.prisma.course.update({
        where: { id: id },
        data: updateCourseDto,
      });
      return { code: 200, status: 'success', message: '课程记录更新成功' };
    } catch (error) {
      console.error('更新课程记录时发生错误:', error);
      return { code: 500, status: 'error', message: error };
    }
  }

 // 删除课程，根据id  
  remove(id: number) {
   try{
    const course = this.prisma.course.findUnique({
        where: { id: id },
    });
    if (!course) {
        return {
            code:400,
            status: 'error',
            message: '未找到要删除的课程记录',
        };
    }
    this.prisma.course.delete({
        where: { id: id },
});
    return { code: 200, status:'success', message: '课程记录删除成功' };
   } catch (error) {
    console.error('删除课程记录时发生错误:', error);
    return { code: 500, status: 'error', message: error };
   }
  }
}

