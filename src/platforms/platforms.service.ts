import { Injectable } from '@nestjs/common';
import { CreatePlatformDto } from './dto/create-platform.dto';
import { UpdatePlatformDto } from './dto/update-platform.dto';
import { PrismaService } from 'src/Pisma/prisma.service';

@Injectable()
export class PlatformsService {
  constructor(private readonly prisma: PrismaService) {}

  // 根据名称查找平台
  findByName(name: string) {
    return this.prisma.platform.findUnique({
      where: {
        name: name,
      },
    });
  }

  // 新增平台
  async create(createPlatformDto: CreatePlatformDto) {
    return this.prisma.platform.create({
      data: createPlatformDto,
    });
  }

  findAll() { 
    return `This action returns all platforms`;
  }

  findOne(id: number) {
    return `This action returns a #${id} platform`;
  }

  //更新平台，根据id
  async update(id: number, updatePlatformDto: UpdatePlatformDto) {
    try {
      const platform = await this.prisma.platform.findUnique({
        where: { id: id },
      });
      if (!platform) {
        return {
          code: 400,
          status: 'error',
          message: '未找到平台记录',
        };
      }
      await this.prisma.platform.update({
        where: { id: id },
        data: updatePlatformDto,
      });
      return { code: 200, status: 'success', message: '平台记录更新成功' };
    } catch (error) {
      console.log('更新平台记录时发生错误', error);
    }
  }

  //删除平台,根据id
  async remove(id: number) {
    try {
      const platform = await this.prisma.platform.findUnique({
        where: { id: id },
      });
      if (!platform) {
        return {
          code: 400,
          status: 'error',
          message: '未找到要删除的平台记录',
        };
      }
      await this.prisma.platform.delete({
        where: { id: id },
      });
      return { code: 200, status: 'success', message: '学生记录删除成功' };
    } catch (error) {
      console.error('删除学生记录时发生错误:', error);
      return { code: 500, status: 'error', message: error };
    }
  }
}
