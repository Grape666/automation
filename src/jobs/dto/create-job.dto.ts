export class CreateJobDto {
    course_id: number;
    job_platform_id: string;
    name: string;
    type: string;
    syllabus_name?: string //大纲名称
    chapter_name?: string //章节名称
    description?: string //作业描述    
    answer_duration?: string //答题时间
    allow_answer_time?: number //允许做答次数
    total_score?: string //学生作业总分
    score_percentage?: number //学生作业成绩占比
    score_type?: string //评分方式 最高分/最后得分 根据平台定
    start_time?: Date //在平台中的作业开始时间
    end_time?: Date //在平台中的作业结束时间
    total_question_num?: string //题目总数量
    question_type?: string //题目类型
    parent_id?: string
}
