import { Injectable } from '@nestjs/common';
import { CreateJobDto } from './dto/create-job.dto';
import { UpdateJobDto } from './dto/update-job.dto';
import { PrismaService } from 'src/Pisma/prisma.service';

@Injectable()
export class JobsService {
  constructor(private readonly prisma: PrismaService) {}
  create(createJobDto: CreateJobDto) {
    const createJobData = {
      ...createJobDto,
      start_time: createJobDto.start_time && new Date(createJobDto.start_time), //在平台中的作业开始时间
      end_time: createJobDto.end_time && new Date(createJobDto.end_time), //在平台中的作业结束时间
      course: {
        connect: {
          id: createJobDto.course_id,
        },
      },
    };
    delete createJobData.course_id;
    if (createJobDto.parent_id) {
      createJobData['parent'] = {
        connect: {
          id: createJobDto.parent_id,
        },
      };
    }
    if (Object.prototype.hasOwnProperty.call(createJobDto, 'parent_id')) {
      delete createJobData.parent_id;
    }
    return this.prisma.job.upsert({
      where: {
        unique_job_platform: {
          course_id: createJobDto.course_id,
          job_platform_id: createJobDto.job_platform_id,
        },
      },
      create: createJobData as Omit<CreateJobDto, 'parent_id'>,
      update: {
        syllabus_name: createJobData.syllabus_name, //大纲名称
        chapter_name: createJobData.chapter_name, //章节名称
        description: createJobData.description, //作业描述
        answer_duration: createJobData.answer_duration, //答题时间
        allow_answer_time: createJobData.allow_answer_time, //允许做答次数
        total_score: createJobData.total_score, // 作业总分
        score_percentage: createJobData.score_percentage, // 学生作业成绩占比
        score_type: createJobData.score_type, // 评分方式 最高分/最后得分 根据平台定
        start_time: createJobData.start_time, //在平台中的作业开始时间
        end_time: createJobData.end_time, //在平台中的作业结束时间
        total_question_num: createJobData.total_question_num, //题目总数量
        question_type: createJobData.question_type, //题目类型
      },
    });
  }

  findAll() {
    return `This action returns all jobs`;
  }

  findOne(id: number) {
    return this.prisma.job.findUnique({
                where: { id: id },
    }) ;
  }
  findOneByPlatformJobId(course_id: number, job_platform_id: string) {
    return this.prisma.job.findUnique({
      where: {
        unique_job_platform: {
          course_id,
          job_platform_id,
        },
      },
    });
  }

  update(id: number, updateJobDto: UpdateJobDto) {
    return this.prisma.job.update({
      where: { id: id },
      data: {
        syllabus_name: updateJobDto.syllabus_name, //大纲名称
        chapter_name: updateJobDto.chapter_name, //章节名称
        description: updateJobDto.description, //作业描述
        answer_duration: updateJobDto.answer_duration, //答题时间
        allow_answer_time: updateJobDto.allow_answer_time, //允许做答次数
        total_score: updateJobDto.total_score, //学生作业总分
        score_percentage: updateJobDto.score_percentage, //学生作业成绩占比
        score_type: updateJobDto.score_type, //评分方式 最高分/最后得分 根据平台定
        start_time:
          updateJobDto.start_time && new Date(updateJobDto.start_time), //在平台中的作业开始时间
        end_time: updateJobDto.end_time && new Date(updateJobDto.end_time), //在平台中的作业结束时间
        total_question_num: updateJobDto.total_question_num, //题目总数量
        question_type: updateJobDto.question_type, //题目类型
      },
    });
  }

  remove(id: number) {
    return `This action removes a #${id} job`;
  }
}
