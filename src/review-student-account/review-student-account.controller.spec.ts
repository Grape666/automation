import { Test, TestingModule } from '@nestjs/testing';
import { ReviewStudentAccountController } from './review-student-account.controller';
import { ReviewStudentAccountService } from './review-student-account.service';

describe('ReviewStudentAccountController', () => {
  let controller: ReviewStudentAccountController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReviewStudentAccountController],
      providers: [ReviewStudentAccountService],
    }).compile();

    controller = module.get<ReviewStudentAccountController>(ReviewStudentAccountController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
