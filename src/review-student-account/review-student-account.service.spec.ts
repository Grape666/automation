import { Test, TestingModule } from '@nestjs/testing';
import { ReviewStudentAccountService } from './review-student-account.service';

describe('ReviewStudentAccountService', () => {
  let service: ReviewStudentAccountService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ReviewStudentAccountService],
    }).compile();

    service = module.get<ReviewStudentAccountService>(ReviewStudentAccountService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
