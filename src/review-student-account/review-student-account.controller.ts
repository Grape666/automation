import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { ReviewStudentAccountService } from './review-student-account.service';
import { CreateReviewStudentAccountDto } from './dto/create-review-student-account.dto';
import { UpdateReviewStudentAccountDto } from './dto/update-review-student-account.dto';
import { QueryReviewDto } from 'src/review/dto/query-review.dto';

@Controller('review-student-account')
export class ReviewStudentAccountController {
  constructor(
    private readonly reviewStudentAccountService: ReviewStudentAccountService,
  ) {}

  @Post()
  create(@Body() createReviewStudentAccountDto: CreateReviewStudentAccountDto) {
    return this.reviewStudentAccountService.create(
      createReviewStudentAccountDto,
    );
  }

 // 导入学生账号评阅记录 
  @Post('import')
  importReview(
    @Body() createReviewStudentAccountDto: CreateReviewStudentAccountDto[],
  ) {
    return this.reviewStudentAccountService.importReview(
      createReviewStudentAccountDto,
    );
  }
// 查询全部学生账号评阅记录
  @Get()
  findAll(@Query() query:QueryReviewDto) {
    return this.reviewStudentAccountService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.reviewStudentAccountService.findOne(+id);
  }

  // 更新学生账号评阅记录
  @Patch(':id')
  update(
    @Param('id') id: string, @Body() updateReviewStudentAccountDto: UpdateReviewStudentAccountDto
  ) {
    return this.reviewStudentAccountService.updateReview(+id,updateReviewStudentAccountDto);
  }

 // 删除学生账号评阅记录
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.reviewStudentAccountService.remove(+id);
    // return {code:500,status:'error',message:'学生账号评阅禁止删除'}
  }
}
