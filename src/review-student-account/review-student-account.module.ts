import { Module } from '@nestjs/common';
import { ReviewStudentAccountService } from './review-student-account.service';
import { ReviewStudentAccountController } from './review-student-account.controller';

@Module({
  controllers: [ReviewStudentAccountController],
  providers: [ReviewStudentAccountService],
})
export class ReviewStudentAccountModule {}
