import { Injectable } from '@nestjs/common';
import { CreateReviewStudentAccountDto } from './dto/create-review-student-account.dto';
import { UpdateReviewStudentAccountDto } from './dto/update-review-student-account.dto';
import { PrismaService } from 'src/Pisma/prisma.service';

@Injectable()
export class ReviewStudentAccountService {
  constructor(private readonly prisma: PrismaService) {}

  create(createReviewStudentAccountDto: CreateReviewStudentAccountDto) {
    return createReviewStudentAccountDto;
  }

  // 导入评阅账号
  importReview(createReviewStudentAccountDto: CreateReviewStudentAccountDto[]) {
    return this.prisma.$transaction((prisma) => {
      return Promise.all(
        createReviewStudentAccountDto.map(async (item) => {
          try {
            const createdStudent = await prisma.reviewStudentAccount.create({
              data: {
                name: item.name && item.name.toString(),
                account: item.account && item.account.toString().trim(),
                organization_id: item.organization_id,
              },
            });
            return {
              code: 200,
              status: 'success',
              message: '导入成功了',
              data: { items: createdStudent },
            };
          } catch (error) {
            if (error.code === 'P2002') {
              return {
                code: 400,
                status: 'error',
                message: '学生账号重复已存在',
                data: { items: item },
              };
            }
            console.error('导入记录时发生错误:', error);
            return { code: 500, status: 'error', message: '导入格式错误' };
          }
        }),
      );
    });
  }

  //查询全部学生账号评阅记录
  async findAll(query) {
    const condition = PrismaService.PagingListQueryGenerator(
      this.prisma.reviewStudentAccount,
      query,
    );
    condition.where = {
      ...condition.where,
      name: { contains: query.name },
      account: { contains: query.account },
      organization_id: Number(query.organization_id),
    };

    const [items, total] = await this.prisma.$transaction([
      this.prisma.reviewStudentAccount.findMany(condition),
      this.prisma.reviewStudentAccount.count({
        where: condition.where,
      }),
    ]);

    return { items, total };
  }

  findOneByAccount(account: string) {
    return this.prisma.reviewStudentAccount.findUnique({
      where: { account: account },
    });
  }

  // 查询一个学生账号评阅记录
  async findOne(id: number) {
    try {
      const reviewStudentAccount =
        await this.prisma.reviewStudentAccount.findUnique({
          where: { id: id },
        });
      if (!reviewStudentAccount) {
        return {
          code: 400,
          status: 'error',
          message: '未找到对应的学生账号评阅记录',
        };
      }
      return reviewStudentAccount;
    } catch (error) {
      console.error('查询学生账号评阅记录时发生错误:', error);
      return { code: 500, status: 'error', message: error };
    }
  }
  // 更新学生账号评阅记录,根据id
  async updateReview(
    id: number,
    updateReviewStudentAccountDto: UpdateReviewStudentAccountDto,
  ) {
    // 检查账号是否存在
    try {
      const existingReview = await this.prisma.reviewStudentAccount.findUnique({
        where: { id: id },
      });

      if (!existingReview) {
        return {
          code: 400,
          status: 'error',
          message: '学生账号评阅记录不存在',
        };
      }

      await this.prisma.reviewStudentAccount.update({
        where: { id: id },
        data: updateReviewStudentAccountDto,
      });
      return {
        code: 200,
        status: 'success',
        message: '学生账号评阅记录更新成功',
      };
    } catch (error) {
      console.error('更新学生账号评阅记录时发生错误:', error);
      return { code: 500, status: 'error', message: error };
    }
  }

  // 删除学生账号评阅记录
  async remove(id: number) {
    try {
      const reviewStudentAccount = this.prisma.reviewStudentAccount.findUnique({
        where: { id: id },
      });

      if (!reviewStudentAccount) {
        return {
          code: 400,
          status: 'error',
          message: '未找到对应的学生账号评阅记录',
        };
      }
      await this.prisma.reviewStudentAccount.delete({
        where: { id: id },
      });
      return {
        code: 200,
        status: 'success',
        message: '删除学生账号评阅记录成功',
      };
    } catch (error) {
      console.error('删除学生账号评阅记录时发生错误:', error);
      return { code: 500, status: 'error', message: error };
    }
  }

  findManyByAccounts(accounts: string[]) {
    return this.prisma.reviewStudentAccount.findMany({
      where: {
        account: {
          in: accounts,
        },
      },
    });
  }
}
