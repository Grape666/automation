export class CreateReviewStudentAccountDto {
  name?: string; //姓名
  account: string; //账号
  organization_id?: number; //机构 id
  created_at: Date; //创建时间
  updated_at: Date; //更新时间
}
