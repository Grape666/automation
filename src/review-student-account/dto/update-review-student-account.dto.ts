import { PartialType } from '@nestjs/swagger';
import { CreateReviewStudentAccountDto } from './create-review-student-account.dto';

export class UpdateReviewStudentAccountDto extends PartialType(CreateReviewStudentAccountDto) {}
