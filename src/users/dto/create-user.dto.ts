export class CreateUserDto {
    name ?: string;
    username: string;
    password: string;
    organization_model : string;
}
