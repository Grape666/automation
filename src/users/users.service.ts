import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { PrismaService } from 'src/Pisma/prisma.service';

// 这应该是一个真正的类/接口，代表一个用户实体
export type User = any;

@Injectable()
export class UsersService {
  constructor(private readonly prisma: PrismaService) {}

//   async saveUsersToDatabase() {
//     for (const user of this.users) {
//       await this.prisma.user.create({
//         data: user,
//       });
//     }
//   }

//   private readonly users = [
//     {
//       userId: 1,
//       username: 'admin',
//       password: '123456',
//       name: '蜂蜜柚子',
//     },
//     {
//       userId: 2,
//       username: 'maria',
//       password: 'guess',
//       name: '百香果',
//     },
//   ];

//   async findOne(username: string): Promise<User | undefined> {
//     return this.users.find((user) => user.username === username);
//   }
    async findOne(username: string): Promise<User | undefined> {
      return this.prisma.user.findUnique({ where: { username } });
    }

  create(body: CreateUserDto) {
    return this.prisma.user.create({
      data: body,
    });
  }
}
