import { Injectable } from '@nestjs/common';
import { CreateOrganizationUserDto } from './dto/create-organization-user.dto';
import { UpdateOrganizationUserDto } from './dto/update-organization-user.dto';
import { PrismaService } from 'src/Pisma/prisma.service';

@Injectable()
export class OrganizationUserService {
  constructor(private readonly prisma: PrismaService) {}

  //创建管理机构
  async create(createOrganizationUserDto: CreateOrganizationUserDto) {
    createOrganizationUserDto.user_id = Number(
      createOrganizationUserDto.user_id,
    );
    createOrganizationUserDto.organization_id = Number(
      createOrganizationUserDto.organization_id,
    );

    return this.prisma.organizationUser.create({
      data: createOrganizationUserDto,
    });
  }

  //查询全部管理机构
    async findAll(query) {
      query.student_id = Number(query.student_id);
      const condition = PrismaService.PagingListQueryGenerator(
        this.prisma.organizationUser,
        query,
      );
      condition.where = {
        user_id: query.user_id && +query.user_id,
        organization_id: query.organization_id && +query.organization_id,
      };
      const [items, total] = await this.prisma.$transaction([
        this.prisma.organizationUser.findMany(condition),
        this.prisma.organizationUser.count({
          where: condition.where,
        }),
      ]);
      return { items, total };
    }

  findOne(id: number) {
    return `This action returns a #${id} organizationUser`;
  }

  //更新管理机构,根据id
  async update(
    id: number,
    updateOrganizationUserDto: UpdateOrganizationUserDto,
  ) {
    try {
      const organizationUser = await this.prisma.organizationUser.findUnique({
        where: { id: id },
      });
      if (!organizationUser) {
        return {
          code: 400,
          status: 'error',
          message: '未找到管理机构记录',
        };
      }
      await this.prisma.organizationUser.update({
        where: { id },
        data: updateOrganizationUserDto,
      });

      return { code: 200, status: 'success', message: '管理机构记录更新成功' };
    } catch (error) {
      console.error('更新管理机构记录时发生错误:', error);
      return { code: 500, status: 'error', message: error };
    }
  }
 //删除管理机构，根据id  
  async remove(id: number) {
    const organizationUser = await this.prisma.organizationUser.findUnique({
      where: { id: id },
    });
    if (organizationUser) {
      return {
        code: 400,
        status: 'error',
        message: '未找到要删除的管理机构记录',
      };
    }
    await this.prisma.organizationUser.delete({
      where: { id: id },
    });
    return { code: 200, status: 'succes', message: '管理机构记录删除成功' };
  }
  catch(error) {
    console.error('消息记录时发生错误:', error);
    return { code: 500, status: 'error', message: error };
  }
}
