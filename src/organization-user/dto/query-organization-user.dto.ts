import { PagingQueryDto } from 'src/Pisma/dto/query.dto';

export class QueryOrganizationUserDto extends PagingQueryDto {
  user_id: number; //用户id
  organization_id: number; //机构id
}
