import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { OrganizationUserService } from './organization-user.service';
import { CreateOrganizationUserDto } from './dto/create-organization-user.dto';
import { UpdateOrganizationUserDto } from './dto/update-organization-user.dto';
import { QueryOrganizationUserDto } from './dto/query-organization-user.dto';

@Controller('organization-user')
export class OrganizationUserController {
  constructor(
    private readonly organizationUserService: OrganizationUserService,
  ) {}

  @Post()
  create(@Body() createOrganizationUserDto: CreateOrganizationUserDto) {
    return this.organizationUserService.create(createOrganizationUserDto);
  }

  @Get()
  findAll(@Query() query: QueryOrganizationUserDto) {
    return this.organizationUserService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.organizationUserService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateOrganizationUserDto: UpdateOrganizationUserDto,
  ) {
    return { code: 500, status: 'error', message: '管理机构禁止修改' };
    // return this.organizationUserService.update(+id, updateOrganizationUserDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return { code: 500, status: 'error', message: '管理机构禁止删除' };
    // return this.organizationUserService.remove(+id);
  }
}
