import * as crypto from 'crypto';
import * as fs from 'fs';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';

// 加密数据
function encryptData(data) {
  const publicKeyPEM = fs.readFileSync("public_public_key.pem", 'utf8');
  const publicKey = crypto.createPublicKey({
      key: publicKeyPEM,
      format: 'pem',
  });
  const encryptedData = crypto.publicEncrypt(
      {
          key: publicKey,
          padding: crypto.constants.RSA_PKCS1_OAEP_PADDING,
          oaepHash: 'sha256',
      },
      Buffer.from(data, 'utf8')
  );
  return encryptedData.toString('hex');
}

// 解密数据
function decryptData(encryptedData) {
  const privateKeyPEM = fs.readFileSync("private_key.pem", 'utf8');
  const privateKey = crypto.createPrivateKey({
      key: privateKeyPEM,
      format: 'pem',
  });
  const decryptedData = crypto.privateDecrypt(
      {
          key: privateKey,
          padding: crypto.constants.RSA_PKCS1_OAEP_PADDING,
          oaepHash: 'sha256',
      },
      Buffer.from(encryptedData, 'hex')
  );

  return decryptedData.toString('utf8');
}

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async signIn(username, pass) {
    username = decryptData(username);
    // 解密密码
    pass = decryptData(pass);
    const user = await this.usersService.findOne(username);
    if (user?.password !== pass) {
      throw new UnauthorizedException();
    }
    const payload = { sub: user.userId, username: user.username };
    return {
      access_token: await this.jwtService.signAsync(payload),
      name: encryptData(user.name),
    };
  }

  async signIn1(username, pass){
    return {
      username:encryptData(username),
      password:encryptData(pass)
    }
  }
}
