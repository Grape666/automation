export class CreateStudentCourseDto {
  student_id: number;
  course_id:number
  formal_scores?: number; //形考分数
  final_score?: number; //终考分数
  totol_score?: number; //总分
  formal_total_score?: number; //形考总分
  formal_percentage?: number; //形考占比
  final_percentage?: number; //终考占比
  schedule?: number; //进度
  course_url?: string; //课程地址
  start_time?: string; //课程开始时间
  end_time?: string; //课程结束时间
  finished_status?: boolean; // 完成状态：true 已完成  false 未完成

}
