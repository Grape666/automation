import { PagingQueryDto } from "src/pisma/dto/query.dto";

export class QueryStudentCourseDto extends PagingQueryDto {
    account?:string;
    student_id?: number;
    name?: string;
    totol_score_gt?: number;  // 大于总分
    totol_score_lt?: number; // 小于总分
    schedule_gt?: number; // 大于进度
    schedule_lt?: number;  // 小于进度
    formal_scores_gt?: number; // 大于形考分数
    formal_scores_lt?: number;  // 小于形考分数
    final_score_gt?: number; // 大于终考分数
    final_score_lt?: number;  // 小于终考分数
}
