import { PartialType } from '@nestjs/swagger';
import { CreateStudentCourseDto } from './create-student-course.dto';

export class UpdateStudentCourseDto extends PartialType(
  CreateStudentCourseDto,
) {
  id?: number;
  schedule?: number;
  run_status?: string; // 运行状态 ： running 运行中 pending 等待运行
  run_count?: number; // 运行次数  0 次代表未运行
}
   