import { Module } from '@nestjs/common';
import { StudentCoursesService } from './student-courses.service';
import { StudentCoursesController } from './student-courses.controller';

@Module({
  controllers: [StudentCoursesController],
  providers: [StudentCoursesService],
  exports:[StudentCoursesService]
})
export class StudentCoursesModule {}
