import { BadRequestException, Injectable } from '@nestjs/common';
import { UpdateStudentCourseDto } from './dto/update-student-course.dto';
import { QueryStudentCourseDto } from './dto/query-student-course.dto';
import { PrismaService } from 'src/Pisma/prisma.service';
import { CreateStudentCourseDto } from './dto/create-student-course.dto';
@Injectable()
export class StudentCoursesService {
  constructor(private readonly prisma: PrismaService) { }

  create(createStudentCourseDto: CreateStudentCourseDto) {
    return this.prisma.studentCourse.upsert({
      where: {
        unique_student_id_name: {
          student_id: createStudentCourseDto.student_id,
          course_id: createStudentCourseDto.course_id,
        },
      },
      create: createStudentCourseDto,
      update: {},
    });
  }

  createOrUpdateStudentCourse(createStudentCourseDto: CreateStudentCourseDto) {
    return this.prisma.studentCourse.upsert({
      where: {
        unique_student_id_name: {
          course_id: createStudentCourseDto.course_id,
          student_id: createStudentCourseDto.student_id,
        },
      },
      create: createStudentCourseDto,
      update: {
        student_id: createStudentCourseDto.student_id || undefined,
        formal_total_score:
          typeof createStudentCourseDto.formal_total_score === 'number' &&
          createStudentCourseDto.formal_total_score,
        final_score:
          (typeof createStudentCourseDto.final_score === 'number' &&
            createStudentCourseDto.final_score) ||
          undefined,
        totol_score:
          (typeof createStudentCourseDto.totol_score === 'number' &&
            createStudentCourseDto.totol_score) ||
          undefined,
        formal_percentage:
          (typeof createStudentCourseDto.formal_percentage === 'number' &&
            createStudentCourseDto.formal_percentage) ||
          undefined,
        final_percentage:
          (typeof createStudentCourseDto.final_percentage === 'number' &&
            createStudentCourseDto.final_percentage) ||
          undefined,
        schedule:
          (typeof createStudentCourseDto.schedule === 'number' &&
            createStudentCourseDto.schedule) ||
          undefined,
      },
    });
  }

  async findAll(query: QueryStudentCourseDto) {
    const findCondition = PrismaService.PagingListQueryGenerator(
      this.prisma.studentCourse.fields,
      query,
    );

    const where = {
      totol_score: {
        gt: query.totol_score_gt,
        lt: query.totol_score_lt,
      },
      schedule: {
        gt: query.schedule_gt,
        lt: query.schedule_lt,
      },
      formal_scores: {
        gt: query.formal_scores_gt,
        lt: query.formal_scores_lt
      },
      final_score: {
        gt: query.final_score_gt,
        lt: query.final_score_lt
      },
      student:{
        account:{
          contains:query.account
        }
      },
      student_id: query.student_id,
      course: {
        name: {
          contains: query.name,
        },

      },
    }
    const count = await this.prisma.studentCourse.count({
      where
    })

    const result = await this.prisma.studentCourse.findMany({
      ...findCondition,
      where,
      include: {
        course: {
          include: {
            jobs: true
          }
        },
        jobs: true,
        student:true
      },
    });

    const new_result = result.map(item => {
      const course_jobs = item.course.jobs
      const student_jobs = item.jobs
      const new_jobs = course_jobs.map(job => {
        for (let i = 0; i < student_jobs.length; i++) {
          if (job.id === student_jobs[i].job_id) {
            return {
              ...job,
              ...student_jobs[i],
            }
          }
        }
      })
      const res = {
        ...item.course,
        ...item,
        jobs: new_jobs
      }
      delete res.course
      return res
    })
    return {
      items: new_result,
      total: count,
    }
  }

    findOneByCourseId(student_id: number, course_id: number) {
      return this.prisma.studentCourse.findUnique({
        where: { 
          unique_student_id_name:{
            student_id,
            course_id
          }
        },
      });
    }



  // 更新学生课程
  updateStudentCourse(updateStudentCourseDto: UpdateStudentCourseDto) {
    let where = undefined;
    if (updateStudentCourseDto.id){
      where ={
        id : updateStudentCourseDto.id
      }
    }else{
      where = {
        unique_student_id_name: {
          course_id: updateStudentCourseDto.course_id,
          student_id: updateStudentCourseDto.student_id,
        },
      }
    }
    return this.prisma.studentCourse
      .update({
        where,
        data: {
          formal_scores:
            (typeof updateStudentCourseDto.formal_scores === 'number' &&
              updateStudentCourseDto.formal_scores) ||
            undefined,
          formal_total_score:
            (typeof updateStudentCourseDto.formal_total_score === 'number' &&
              updateStudentCourseDto.formal_total_score) ||
            undefined,
          final_score:
            (typeof updateStudentCourseDto.final_score === 'number' &&
              updateStudentCourseDto.final_score) ||
            undefined,
          totol_score:
            (typeof updateStudentCourseDto.totol_score === 'number' &&
              updateStudentCourseDto.totol_score) ||
            undefined,
          formal_percentage:
            (typeof updateStudentCourseDto.formal_percentage === 'number' &&
              updateStudentCourseDto.formal_percentage) ||
            undefined,
          final_percentage:
            (typeof updateStudentCourseDto.final_percentage === 'number' &&
              updateStudentCourseDto.final_percentage) ||
            undefined,
          schedule:
            (typeof updateStudentCourseDto.schedule === 'number' &&
              updateStudentCourseDto.schedule) ||
            undefined,
          finished_status:
            (typeof updateStudentCourseDto.finished_status === 'boolean' &&
              updateStudentCourseDto.finished_status) ||
            undefined,
          run_status: updateStudentCourseDto.run_status || undefined,
          run_count: {
            increment: updateStudentCourseDto.run_count || 0,
          },
        },
      })
      .catch((e) => {
        console.error(e);
        throw new BadRequestException('学生账号不存在');
      });
  }

  remove(id: number) {
    return this.prisma.studentCourse.delete({
      where: { id },
    });
  }

  update(id: number, updateStudentCourseDto: UpdateStudentCourseDto) {
    return this.prisma.studentCourse.update({
      where: {
        id,
      },
      data: updateStudentCourseDto,
    });
  }

  createMany(courses: CreateStudentCourseDto[]) {
    return this.prisma.studentCourse.createMany({
      data: courses,
      skipDuplicates: true,
    });
  }
}
