import { Body, Controller, Get,  Post, Query } from "@nestjs/common";
import { CreateReviewRecordDto } from "./dto/create-review-record-dto";
import { RobotReviewRecordService } from "./robot.review-record.service";


@Controller('robot/reviews')
export class RobotReviewRecordController {
    constructor(
        private readonly robotReviewRecordService:RobotReviewRecordService
    ) { }
    @Post()
    async createReviewRecord(@Body() reviewRecord: CreateReviewRecordDto) {
    
        return this.robotReviewRecordService.createReviewRecord(reviewRecord)
    }
    @Get("students")
    async getStudentAccounts(@Query("accounts") accounts: string) {
        return this.robotReviewRecordService.getStudentAccounts(JSON.parse(accounts))
    }

}