import { Injectable } from "@nestjs/common";
import { CoursesService } from "src/courses/courses.service";
import { JobsService } from "src/jobs/jobs.service";
import { ReviewStudentAccountService } from "src/review-student-account/review-student-account.service";
import { ReviewService } from "src/review/review.service";
import { StudentCoursesService } from "src/student-courses/student-courses.service";
import { StudentJobService } from "src/student-job/student-job.service";
import { StudentsService } from "src/students/students.service";
import { CreateReviewRecordDto } from "./dto/create-review-record-dto";

@Injectable()
export class RobotReviewRecordService {
  constructor(
    private readonly reviewService: ReviewService,
    private readonly courseService: CoursesService,
    private readonly studentService: StudentsService,
    private readonly jobService: JobsService,
    private readonly reviewStudentAccountService: ReviewStudentAccountService,
    private readonly studentCourseService: StudentCoursesService,
    private readonly studentJobService: StudentJobService,
  ) { }

  async createReviewRecord(reviewRecord: CreateReviewRecordDto) {
    const reviewStudentAccount = await this.reviewStudentAccountService.findOneByAccount(reviewRecord.student_account)
    const student = await this.studentService.findByAccount(reviewRecord.student_account, 1)
    const course = await this.courseService.findOneByPlatformCourseId(reviewRecord.platform_course_id, 1)
    const job = course && await this.jobService.findOneByPlatformJobId(course.id, reviewRecord.platform_job_id)
    const studentCourse = student && course && await this.studentCourseService.findOneByCourseId(student.id, course.id)
    const studentJob = studentCourse && job && await this.studentJobService.findOneByStudentCourseIdAndJobId(studentCourse.id, job.id)
    let  score :number | undefined;
    
    if ((reviewRecord.score || typeof reviewRecord.score === 'number') &&  reviewRecord.score.toString().trim() ){
      score = parseFloat(reviewRecord.score.toString().trim())
    }
     

    return this.reviewService.create({
      name: reviewRecord.name,
      school_name: reviewRecord.school_name,
      student_account: reviewRecord.student_account,
      platform_course_id: reviewRecord.platform_course_id,
      platform_job_id: reviewRecord.platform_job_id,
      course_id:course && course.id,
      job_id: job && job.id,
      status: reviewRecord.status,
      score,
      review_student_account_id: reviewStudentAccount && reviewStudentAccount.id,
      student_id: student && student.id,
      student_course_id: studentCourse && studentCourse.id,
      student_job_id: studentJob && studentJob.id,
    })
  }
  async getStudentAccounts(accounts: string[]) {
    accounts = accounts.map(account => account.trim())
    const studentAccounts = await this.studentService.findManyByAccounts(accounts);
    const reviewStudentAccounts = await this.reviewStudentAccountService.findManyByAccounts(accounts);
    const studentSet = new Set();
    studentAccounts.forEach(student => studentSet.add(student.account))
    reviewStudentAccounts.forEach(reviewStudentAccount => studentSet.add(reviewStudentAccount.account))
    return Array.from(studentSet)
  }
}