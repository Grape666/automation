import { Injectable } from '@nestjs/common';
import { StudentCoursesService } from 'src/student-courses/student-courses.service';
import {  SyncStudentCourse, UnifyCourseDto } from './dto/sync-student-course.dto';
import {
  StartStudentCourse,
  StopStudentCourse,
} from './dto/start-student-course.dto';
import { CoursesService } from 'src/courses/courses.service';
import { UpdateStudentCourseDto } from 'src/student-courses/dto/update-student-course.dto';
import { CourseRunLogService } from 'src/course-run-log/course-run-log.service';

@Injectable()
export class RobotStudentCourseService {
  constructor(
    private readonly coursesService: CoursesService,
    private readonly studentCoursesService: StudentCoursesService,
    private readonly courseRunLogService :CourseRunLogService
  ) {}

  // 更新学生课程
  updateStudentCourse(id:number,body: UpdateStudentCourseDto) {
    return this.studentCoursesService.update(id,body);
  }

  // 同步学生课程
  async syncStudentCourse(body: SyncStudentCourse) {
    const result = []
    for (const item of body.student_courses) {
      const course = await this.coursesService.create({
        ...item,
        start_time: item.start_time &&  new Date(item.start_time),
        platform_id:body.platform_id,
      })
      const studentCourse = await this.studentCoursesService.create({
        course_id:course.id,
        student_id:body.student_id
      })
      const data = {
        ...course,
        ...studentCourse,
        course_id : course.id,
        student_course_id : studentCourse.id
      }
      delete data.id;
      result.push(data)
    }
    return result
  }


  unifyStudentCourse(body:UnifyCourseDto){
   return this.coursesService.create({
      ...body,
      start_time: body.start_time &&  new Date(body.start_time),
      platform_id:body.platform_id,
    })
  }

  // 课程开始
 async startStudentCourse(id :number,body: StartStudentCourse) {
    const studentCourse = await  this.studentCoursesService.updateStudentCourse({
      id,
      schedule: body.schedule,
      run_count: 1,
      run_status: 'running',
    });
   const courseRunLog = await this.courseRunLogService.create({
      student_run_log_id: body.runing_id,
      course_id: studentCourse.id,
      start_time: new Date(),
    })
    studentCourse["course_run_log"] = courseRunLog;
    return studentCourse
  }

  // 课程结束
  async stopStudentCourse(body: StopStudentCourse) {
    const courseRunLog = await this.courseRunLogService.update(body.course_run_log_id, {
      end_time: new Date(),
      remark: body.remark,
      result: body.result,
    })
    return this.studentCoursesService.updateStudentCourse({
      id:courseRunLog.course_id,
      run_status: 'pending',
    });
  }
}
