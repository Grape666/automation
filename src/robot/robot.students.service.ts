import { Injectable } from '@nestjs/common';
import { StudentsService } from '../students/students.service';
import { StudentRunLogService } from 'src/student-run-log/student-run-log.service';

@Injectable()
export class RobotStudentsService {
  constructor(private readonly studentsService: StudentsService,
    private readonly studentRunLogService: StudentRunLogService
  ) { }

  updateByAccount(
    account: string,
    platform_id: number,
    name: string,
    major: string,
    password: string,
  ) {
    return this.studentsService.updateByAccount({
      account,
      platform_id,
      name,
      major,
      password,
    });
  }

  async start(account: string, platform_id: number, ip: string, devicename: string, LAN_IP: string,name:string, password:string, organization_id:number) {
    const student = await this.studentsService.createOrUpdateByAccount(account,{
      name,
      password,
      organization_id,
      account,
      platform_id: platform_id,
      run_count: 1,
      run_status: 'running',
      calculate_status: false,
      study_season:'2024春季'
    });
    const studentRunLog = await this.studentRunLogService.create({
      student_id: student.id,
      user_id: 1, // 默认给admin用户
      ip: ip,
      devicename: devicename,
      LAN_IP: LAN_IP,
      start_time: new Date(),
    })
    student["studentRunLog"] = studentRunLog
    return student;
  }

  async stop(account: string, platform_id: number, student_run_id: number, result: string, remark: string) {
    await this.studentRunLogService.update(student_run_id, {
      end_time: new Date(),
      result: result,
      remark: remark,
    })
    return await this.studentsService.updateByAccount({
      account, platform_id,
      run_status: 'pending',
      calculate_status: false,
    })
  }


  // 更新登陆状态
  updateLoginedStatus(
    account: string,
    platform_id: number,
    logined: string,
    remark: string,
  ) {
    return this.studentsService.updateByAccount({
      account,
      platform_id: platform_id,
      login_status: logined,
      remark: remark,
    });
  }
}
