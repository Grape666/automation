import { Injectable } from '@nestjs/common';
import { StudentJobService } from 'src/student-job/student-job.service';
import { JobsService } from 'src/jobs/jobs.service';
import { CreateJobDto } from 'src/jobs/dto/create-job.dto';
import { UpdateStudentJobDto } from 'src/student-job/dto/update-student-job.dto';
import { UnifyStudentJobDto } from './dto/unify-student-job.dto';
import { CoursesService } from 'src/courses/courses.service';
@Injectable()
export class RobotStudentJobService {
  constructor(
    private readonly studentJobService: StudentJobService,
    private readonly coursesService: CoursesService,
    private readonly jobsService: JobsService,

  ) { }

  // 同步学生作业
  async syncStudentJobs(student_course_id: number, body: CreateJobDto) {
    if (body.start_time) {
      body.start_time = new Date(body.start_time)
    }
    if (body.end_time) {
      body.end_time = new Date(body.end_time)
    }
    const job = await this.jobsService.create(body);

    const studentJob = await this.studentJobService.create({
      student_course_id,
      job_id: job.id,
    });


    const result = {
      ...job,
      ...studentJob,
    }
    delete result.id;
    result['job_id'] = job.id;
    result['student_job_id'] = studentJob.id;

    return result;
  }

  async unifyStudentJob(body: UnifyStudentJobDto) {
    const course = await this.coursesService.findOneByPlatformCourseId(body.platform_course_id, 1)
    body["course_id"] = course.id;
    delete body.platform_course_id;
    if (body.platform_parent_id) {
      const parentJob = await this.jobsService.findOneByPlatformJobId(course.id, body.platform_parent_id)
      body["parent_id"] = parentJob.id;
      delete body.platform_parent_id;
    }
    return this.jobsService.create(body as unknown as CreateJobDto);
  }



  async update(id: number, body: UpdateStudentJobDto) {
    return this.studentJobService.update(id, body)
  }
}
