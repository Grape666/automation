import {
  Controller,
  Post,
  Patch,
  Body,
  ParseIntPipe,
  Ip,
} from '@nestjs/common';
import { RobotStudentsService } from './robot.students.service';

@Controller('robot/students')
export class RobotStudentsController {
  constructor(
    private readonly robotService: RobotStudentsService,
  ) {}
  /**
   * 开始挂课调用的接口
   * @param account 开始的学生账号
   * @param platform_id 平台id
   * @returns 学生账号信息
   */
  @Post('start')
  createOrUpdateByAccount(
    @Body('account') account: string,
    @Body('platform_id', ParseIntPipe) platform_id: number,
    @Ip() ip: string,
    @Body("devicename") devicename: string,
    @Body('LAN_IP') LAN_IP: string,
    @Body('name') name: string,
    @Body('password') password: string,
    @Body('organization_id') organization_id: number,
  ) {
    return this.robotService.start(account, platform_id,ip,devicename,LAN_IP,name, password, organization_id);
  }

  @Post('stop')
  stopByAccount(
    @Body('account') account: string,
    @Body('platform_id', ParseIntPipe) platform_id: number,
    @Body("student_run_id") student_run_id: number,
    @Body('result') result: string,
    @Body('remark') remark: string,
  ) {
    return this.robotService.stop(account, platform_id,student_run_id,result,remark);
  }

  /**
   * 挂机登录后调用的接口
   * @param account 登录的学生账号
   * @param platform_id 平台id
   * @param status 登录状态
   * @param remark 登录备注
   * @returns 学生账号信息
   */
  @Patch('login-status')
  updateLoginedStatus(
    @Body('account') account: string,
    @Body('platform_id', ParseIntPipe) platform_id: number,
    @Body('logined') status: string,
    @Body('remark') remark: string,
  ) {
    return this.robotService.updateLoginedStatus(
      account,
      +platform_id,
      status,
      remark,
    );
  }

// 更新学生、专业、密码
  @Patch('info')
  updateStudentInfo(
    @Body('account') account: string,
    @Body('platform_id', ParseIntPipe) platform_id: number,
    @Body('name') name: string,
    @Body('major') major: string,
    @Body('password') password: string,
  ) {
    return this.robotService.updateByAccount(account ,platform_id,name, major, password);
  }
} 