import { Body, Controller, Param, ParseIntPipe, Patch, Post } from '@nestjs/common';
import { RobotStudentJobService } from './robot.students-job.service';
import { CreateJobDto } from 'src/jobs/dto/create-job.dto';
import { UpdateStudentJobDto } from 'src/student-job/dto/update-student-job.dto';
import { UnifyStudentJobDto } from './dto/unify-student-job.dto';


@Controller('robot/jobs')
export class RobotStudentsJobController {
  constructor(private readonly robotService:RobotStudentJobService) {}

  // 同步学生作业
  @Post('sync/:student_course_id')
  syncStudentJobs(@Param('student_course_id',ParseIntPipe) student_course_id: number, @Body() body: CreateJobDto) {
    return this.robotService.syncStudentJobs(student_course_id,body);
  }

  // 更新学生作业
  @Post('unify')
  unifyStudentJob(@Body() body: UnifyStudentJobDto) {
    return this.robotService.unifyStudentJob(body);
  }


  @Patch(':id')
  updata(@Param('id',ParseIntPipe) id: number, @Body() body :UpdateStudentJobDto){
    return this.robotService.update(id,body);
  }
}