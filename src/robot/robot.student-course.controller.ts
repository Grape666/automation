import { Controller, Patch, Body, Post, ParseIntPipe, Param } from '@nestjs/common';

import { RobotStudentCourseService } from './robot.students-course.service';
import { SyncStudentCourse, UnifyCourseDto } from './dto/sync-student-course.dto';
import {
  StartStudentCourse,
  StopStudentCourse,
} from './dto/start-student-course.dto';

import { UpdateStudentCourseDto } from 'src/student-courses/dto/update-student-course.dto';
@Controller('robot/courses')
export class RobotStudentsCourseController {
  constructor(private readonly robotService: RobotStudentCourseService) {}


  @Patch(":id")
  updateStudentCourse(@Param('id', ParseIntPipe) id :number,@Body() body: UpdateStudentCourseDto) {
    return this.robotService.updateStudentCourse(id,body);
  }


  // 同步作业课程
  @Post('sync')
  syncStudentCourse(@Body() body: SyncStudentCourse) {
    return this.robotService.syncStudentCourse(body);
  }


  /**
   * 统一学生课程，扩展端使用
   * @returns {Promise<any>}
   */
  @Post('unify')
  unifyCourse(@Body() body:UnifyCourseDto) {
    return this.robotService.unifyStudentCourse(body);
  }

  // 课程开始
  @Post('start/:id')
  startStudentCourse(@Param('id', ParseIntPipe) id :number,@Body() body: StartStudentCourse) {
    return this.robotService.startStudentCourse(id,body);
  }

  // 课程结束
  @Post('stop')
  stopStudentCourse(@Body() body: StopStudentCourse) {
    return this.robotService.stopStudentCourse(body);
  }
}
