import { Module } from '@nestjs/common';
import { RobotStudentsCourseController } from './robot.student-course.controller';
import { RobotStudentsController } from './robot.students.controller';
import { RobotStudentCourseService } from './robot.students-course.service';
import { RobotStudentsService } from './robot.students.service';
import { StudentsService } from 'src/students/students.service';
import { StudentCoursesService } from 'src/student-courses/student-courses.service';
import { CoursesService } from 'src/courses/courses.service';
import { RobotStudentsJobController } from './robot.student-job.controller';
import { RobotStudentJobService } from './robot.students-job.service';
import { StudentJobService } from 'src/student-job/student-job.service';
import { JobsService } from 'src/jobs/jobs.service';
import { StudentRunLogService } from 'src/student-run-log/student-run-log.service';
import { CourseRunLogService } from 'src/course-run-log/course-run-log.service';
import { ReviewService } from 'src/review/review.service';
import { ReviewStudentAccountService } from 'src/review-student-account/review-student-account.service';
import { RobotReviewRecordController } from './robot.review-record.controller';
import { RobotReviewRecordService } from './robot.review-record.service';
  
 
 
 

 
 
@Module({
  imports: [],
  controllers: [RobotStudentsController, RobotStudentsCourseController, RobotStudentsJobController,RobotReviewRecordController],
  providers: [
    RobotStudentsService,
    RobotStudentCourseService,
    StudentsService,
    StudentCoursesService,
    CoursesService,
    RobotStudentJobService,
    StudentJobService,
    JobsService,
    StudentRunLogService,
    CourseRunLogService,
    ReviewService,
    ReviewStudentAccountService,
    RobotReviewRecordService
  ],
})
export class RobotModule { }
