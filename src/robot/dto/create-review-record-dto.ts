export class CreateReviewRecordDto {
    name?: string; //老师账号的名称
    school_name?: string; // 学校名称
    student_account: string;//学生账号
    platform_course_id: string;//平台课程id
    platform_job_id: string;// 平台的作业id
    // 评阅状态 未交/已交
    status: string;
    score: number; //评阅成绩
}