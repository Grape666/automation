export class StartStudentCourse {
  runing_id: number;
  schedule: number;
}

export class StopStudentCourse {
  course_run_log_id: number;
  remark: string;
  result: string;
}
