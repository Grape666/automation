import { CreateJobDto } from "src/jobs/dto/create-job.dto";
import { PartialType ,OmitType} from '@nestjs/swagger';

export class UnifyStudentJobDto extends PartialType(OmitType(CreateJobDto,['course_id','parent_id'])){
    platform_course_id:string;
    platform_parent_id?:string;
}