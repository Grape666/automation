export class StudentCourse{
    name:string
    course_url:string
    start_time: number; // 时间戳
    platform_course_id: string;
}


export class SyncStudentCourse{
    student_id: number;
    platform_id:number
    student_courses : StudentCourse[];
}

export class UnifyCourseDto  extends StudentCourse{
    platform_id:number;
}