import { Module } from '@nestjs/common';
import { StudentMessageService } from './student-message.service';
import { StudentMessageController } from './student-message.controller';

@Module({
  controllers: [StudentMessageController],
  providers: [StudentMessageService],
})
export class StudentMessageModule {}
