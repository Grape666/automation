import { Test, TestingModule } from '@nestjs/testing';
import { StudentMessageController } from './student-message.controller';
import { StudentMessageService } from './student-message.service';

describe('StudentMessageController', () => {
  let controller: StudentMessageController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StudentMessageController],
      providers: [StudentMessageService],
    }).compile();

    controller = module.get<StudentMessageController>(StudentMessageController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
