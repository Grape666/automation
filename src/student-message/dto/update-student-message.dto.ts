import { PartialType } from '@nestjs/swagger';
import { CreateStudentMessageDto } from './create-student-message.dto';

export class UpdateStudentMessageDto extends PartialType(CreateStudentMessageDto) {}
