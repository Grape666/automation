export class CreateStudentMessageDto {
  student_id: number; //学生id
  student_course_id?: number; // 学生课程id
  student_job_id?: number; //学生作业id
  level: string; //学生消息等级
  title: string; //消息标题
  content: string; //消息内容
  is_read: boolean; // 是否已读
}
