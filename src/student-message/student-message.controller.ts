import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { StudentMessageService } from './student-message.service';
import { CreateStudentMessageDto } from './dto/create-student-message.dto';
import { UpdateStudentMessageDto } from './dto/update-student-message.dto';
import { QueryStudentMessageDto } from './dto/query-student-message.dto';

@Controller('student-message')
export class StudentMessageController {
  constructor(private readonly studentMessageService: StudentMessageService) {}

  @Post()
  create(@Body() createStudentMessageDto: CreateStudentMessageDto) {
    return this.studentMessageService.create(createStudentMessageDto);
  }

  @Get()
  findAll(@Query() query: QueryStudentMessageDto) {
    return this.studentMessageService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.studentMessageService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateStudentMessageDto: UpdateStudentMessageDto,
  ) {
    return this.studentMessageService.update(+id, updateStudentMessageDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return { code: 500, status: 'error', message: id+'消息禁止删除' };
    // return this.studentMessageService.remove(+id);
  }
}
