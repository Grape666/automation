import { Injectable } from '@nestjs/common';
import { CreateStudentMessageDto } from './dto/create-student-message.dto';
import { UpdateStudentMessageDto } from './dto/update-student-message.dto';
import { PrismaService } from 'src/Pisma/prisma.service';

@Injectable()
export class StudentMessageService {
  constructor(private readonly prisma: PrismaService) {}

  // 创建消息
  async create(createStudentMessageDto: CreateStudentMessageDto) {
    createStudentMessageDto.student_id =
      createStudentMessageDto.student_id &&
      Number(createStudentMessageDto.student_id);

    createStudentMessageDto.is_read = Boolean(createStudentMessageDto.is_read);

    return this.prisma.studentMessage.create({
      data: createStudentMessageDto,
    });
  }
  //查询全部消息
  async findAll(query) {
    query.student_id = Number(query.student_id);

    const condition = PrismaService.PagingListQueryGenerator(
      this.prisma.studentMessage,
      query,
    );
    condition.where = {
      student_id: query.student_id,
      level: {
        contains: query.level,
      },
      title: {
        contains: query.title,
      },
      content: {
        contains: query.content,
      },
      is_read: {
        contains: query.is_read,
      },
    };
    const [items, total] = await this.prisma.$transaction([
      this.prisma.studentMessage.findMany(condition),
      this.prisma.studentMessage.count({
        where: condition.where,
      }),
    ]);
    return { items, total };
  }
  findOne(id: number) {
    return `This action returns a #${id} studentMessage`;
  }
  //更新消息，根据id
  async update(id: number, updateStudentMessageDto: UpdateStudentMessageDto) {
    try {
      const studentMessage = await this.prisma.studentMessage.findUnique({
        where: { id: id },
      });
      if (!studentMessage) {
        return {
          code: 400,
          status: 'error',
          message: '未找到消息记录',
        };
      }
      await this.prisma.studentMessage.update({
        where: { id },
        data: updateStudentMessageDto,
      });

      return { code: 200, status: 'success', message: '消息记录更新成功' };
    } catch (error) {
      console.error('更新消息记录时发生错误:', error);
      return { code: 500, status: 'error', message: error };
    }
  }
  // 删除消息，根据id
  async remove(id: number) {
    try {
      const studentMessage = await this.prisma.studentMessage.findUnique({
        where: { id: id },
      });
      if (!studentMessage) {
        return {
          code: 400,
          status: 'error',
          message: '未找到要删除的消息记录',
        };
      }
      await this.prisma.studentMessage.delete({
        where: { id: id },
      });
      return { code: 200, status: 'success', message: '消息记录删除成功' };
    } catch (error) {
      console.error('消息记录时发生错误:', error);
      return { code: 500, status: 'error', message: error };
    }
  }
}
