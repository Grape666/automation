import { Test, TestingModule } from '@nestjs/testing';
import { CourseDisposeLogService } from './course-dispose-log.service';

describe('CourseDisposeLogService', () => {
  let service: CourseDisposeLogService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CourseDisposeLogService],
    }).compile();

    service = module.get<CourseDisposeLogService>(CourseDisposeLogService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
