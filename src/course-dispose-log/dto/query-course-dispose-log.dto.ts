import { PagingQueryDto } from 'src/Pisma/dto/query.dto';

export class QueryCourseDisposeLogDto extends PagingQueryDto {
  student_id: number; //学生id
  user_id: number; //用户id, 挂课账号的id
  devicename?: string; //设备名称
  LAN_IP?: string; //本地ip
  ip?: string; //自己ip
  start_time?: Date; //开始时间
  end_time?: Date; //结束时间
  remark?: string; //备注
  result?: string; //结果
}
