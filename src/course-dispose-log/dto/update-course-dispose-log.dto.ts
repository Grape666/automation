import { PartialType } from '@nestjs/swagger';
import { CreateCourseDisposeLogDto } from './create-course-dispose-log.dto';

export class UpdateCourseDisposeLogDto extends PartialType(CreateCourseDisposeLogDto) {}
