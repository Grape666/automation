import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { CourseDisposeLogService } from './course-dispose-log.service';
import { CreateCourseDisposeLogDto } from './dto/create-course-dispose-log.dto';
import { UpdateCourseDisposeLogDto } from './dto/update-course-dispose-log.dto';
import { QueryCourseDisposeLogDto } from './dto/query-course-dispose-log.dto';

@Controller('course-dispose-log')
export class CourseDisposeLogController {
  constructor(private readonly courseDisposeLogService: CourseDisposeLogService) {}

  @Post()
  create(@Body() createCourseDisposeLogDto: CreateCourseDisposeLogDto) {
    return this.courseDisposeLogService.create(createCourseDisposeLogDto);
  }

  @Get()
  findAll(@Query() query:QueryCourseDisposeLogDto) {
    return this.courseDisposeLogService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.courseDisposeLogService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCourseDisposeLogDto: UpdateCourseDisposeLogDto) {
    return this.courseDisposeLogService.update(+id, updateCourseDisposeLogDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return { code: 500, status: 'error', message: '挂机记录禁止删除' };
    // return this.courseDisposeLogService.remove(+id);
  }
}
