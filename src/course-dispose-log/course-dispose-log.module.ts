import { Module } from '@nestjs/common';
import { CourseDisposeLogService } from './course-dispose-log.service';
import { CourseDisposeLogController } from './course-dispose-log.controller';

@Module({
  controllers: [CourseDisposeLogController],
  providers: [CourseDisposeLogService],
})
export class CourseDisposeLogModule {}
