import { Injectable } from '@nestjs/common';
import { CreateCourseDisposeLogDto } from './dto/create-course-dispose-log.dto';
import { UpdateCourseDisposeLogDto } from './dto/update-course-dispose-log.dto';
import { PrismaService } from 'src/Pisma/prisma.service';

@Injectable()
export class CourseDisposeLogService {
  constructor(private readonly prisma: PrismaService) {}
  // 创建挂机记录
  async create(createCourseDisposeLogDto: CreateCourseDisposeLogDto) {
    createCourseDisposeLogDto.student_id = Number(
      createCourseDisposeLogDto.student_id,
    );
    createCourseDisposeLogDto.user_id = Number(
      createCourseDisposeLogDto.user_id,
    );

    return this.prisma.studentRunLog.create({
      data: createCourseDisposeLogDto,
    });
  }
  // 查询挂机记录
  async findAll(query) {
    query.student_id = Number(query.student_id);
    query.user_id = Number(query.user_id);

    const condition = PrismaService.PagingListQueryGenerator(
      this.prisma.studentRunLog,
      query,
    );
    condition.where = {
      student_id: query.student_id,
      user_id: query.user_id,
      devicename: {
        contains: query.devicename,
      },
      LAN_IP: {
        contains: query.LAN_IP,
      },
    };
    const [items, total] = await this.prisma.$transaction([
      this.prisma.studentRunLog.findMany(condition),
      this.prisma.studentRunLog.count({
        where: condition.where,
      }),
    ]);


    return { items, total };
  }

  findOne(id: number) {
    return `This action returns a #${id} courseDisposeLog`;
  }

  // 更新挂机记录，根据id
  async update(id: number,updateCourseDisposeLogDto: UpdateCourseDisposeLogDto) {
    try {
      const courseDisposeLog = await this.prisma.studentRunLog.findUnique({
        where: { id: id },
      });

      if (!courseDisposeLog) { 
        return {
          code: 400,
          status: 'error',
          message: '未找到消息记录',
        };
      }
      await this.prisma.studentRunLog.update({
        where: { id },
        data: updateCourseDisposeLogDto,
      });

      return { code: 200, status: 'success', message: '消息记录更新成功' };
    } catch (error) {
      console.error('更新消息记录时发生错误:', error);
      return { code: 500, status: 'error', message: error };
    }
  }
//  删除挂机记录,根据id
  async remove(id: number) {
    try {
      const courseDisposeLog = await this.prisma.studentRunLog.findUnique({
        where: { id: id },
      });
      if (!courseDisposeLog) {
        return {
          code: 400,
          status: 'error',
          message: '未找到要删除的挂课记录',
        };
      }
      await this.prisma.studentRunLog.delete({
        where: { id: id },
      });
      return { code: 200, status: 'success', message: '挂课记录删除成功' };
    } catch (error) {
      console.error('挂课记录时发生错误:', error);
      return { code: 500, status: 'error', message: error };
    }
  }

}
