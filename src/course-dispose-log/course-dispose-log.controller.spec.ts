import { Test, TestingModule } from '@nestjs/testing';
import { CourseDisposeLogController } from './course-dispose-log.controller';
import { CourseDisposeLogService } from './course-dispose-log.service';

describe('CourseDisposeLogController', () => {
  let controller: CourseDisposeLogController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CourseDisposeLogController],
      providers: [CourseDisposeLogService],
    }).compile();

    controller = module.get<CourseDisposeLogController>(CourseDisposeLogController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
